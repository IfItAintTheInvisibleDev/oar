# OAR Format

The main elements of the archive are as follows:

* HED: A header with basic information including the sizes of elements of the oar file, the number of images stored in the archive and basic information related to the images such as the size of their encoding information
* DLT: Data Lookup Table, an array of 32 bit LSB integers (one for every sector of every image contained in the archive) that define where within the Unique Sector Buckets (USBs) the data section for a sector is located
* ENC: All the encoding information for the contained images are stored after the DLT. Each filetype that has special handling has its own encoding format
* PAD: Padding of zero bytes until at the next 2 KiB boundary. This is to align USB2048 to the HDD/SSD sector boundary which should help optimise decoding performance
* USB: Unique Sector Buckets. These buckets contain the unique data portion of sectors in the order they were first encountered. Most of the time there is only one bucket type, USB2048, which is for data portions of length 2048 bytes. Three other buckets may exist (USB2324, USB2336, USB2352) if the archive contains BIN images, as BIN images may contain data portions with these lengths (notably audio tracks use all 2352 bytes of a sector for data, so they are stored in USB2352)
* SHA: SHA256 hashes for each of the seven elements in the archive in order (HED, DLT, ENC, USB2048, USB2324, USB2336, USB2352), with zeroes for a particular USB if it's not present

## Basic Decoding Process

The DLT is used to find where in a USB a particular sectors data section is stored. This data section is combined with the encoding information found in ENC to recreate the original sector warts and all.

## ENC Explained

Each image type has it's own encoding format for specialised modelling of that image type. Information in the header defines where in ENC the encoding for a particular image is.

### ISO Encoding

ISO images have no encoding information, the sectors already contain only user data ready for processing.

### BIN Encoding

BIN images use YBE encoding to model the non-user-data elements of the sectors. tl;dr YBE encoding for a sector is comprised of a byte that defines what can be modelled followed by the non-data elements that could not be modelled in the order they normally appear in the raw sector. YBE encoding for an image is simply the sector encodings concatenated.

#### YBE Encoding Type Byte

This byte encapsulates what type of sector we are dealing with and what can be modelled for this particular sector. Because some sector types are easier to model than others a variable number of bits are used to determine the sector type; this gives the more complicated sector types an extra bit to be used to identify a modellable element. The supported sector types are those defined up to extension CD ROM XA, namely Mode 0, Mode 1, Mode 2 (Formless, Form 1, Form 2) and RAW (normally audio)

##### Elements of a sector

To understand the type byte and the flags it contains we need to understand the different elements that could be in a CD ROM XA sector:
* Sync: 12 bytes at the start of a data sector, it should be 00 FF FF FF FF FF FF FF FF FF FF 00 and for YBE encoding purposes it always is (if it's malformed the sector is treated as RAW). This is used to determine sector type so requires no flag
* Address: 3 bytes holding the sector address in BCD format. When parsing an image the sector index is used to generate the Address which models a typical track 1 image. The flag is shown below as A
* Mode byte: This is 0 for a Mode 0 sector, 1 for a Mode 1 sector, or 2 for a Mode 2 sector (Formless, Form 1 and Form 2). This is used to determine sector type so requires no flag
* ECC P: This is the first chunk of ECC data. The flag is shown below as P
* ECC Q: The second chunk of ECC data. The flag is shown below as Q
* EDC: The error detection code present on Mode 1 and Mode 2 Form 1 sectors. The flag is shown below as E
* Subheader: 8 bytes (normally 4 repeated) present on Mode 2 Form 1 and Mode 2 Form 2. Expected value on Form 1 sectors 00 00 01 00 00 00 01 00, expected value on Form 2 sectors 00 00 02 00 00 00 02 00. The flag is shown below as H
* Intermediate: 8 bytes found on Mode 1 sectors, expected value is 00 00 00 00 00 00 00 00. The flag is shown below as I
* Scrambled: Sectors are stored scrambled on disc, one of the necessary features of the physical medium. If a sector has the correct sync but fails the mode check, it is descrambled. If it then passes the mode check it is treated as a scrambled sector, otherwise it is treated as RAW. The flag is shown below as S

##### Structure of YBE type byte

Numbers indicate fixed values determining the sector type. Letters indicate flags that are set if the corresponding element could not be modelled. U indicates an unused flag.

```
76543210
SAIEQP00: Mode 1
SAHEQP01: Mode 2 Form 1
SAHUUU10: Mode 2 Form 2
SAUU0011: Mode 0
UUUU0111: RAW
SAUU1011: Mode 2 Formless
```

### GameCube Encoding

The encoding for a GameCube image is a bit array that defines which sectors have modellable padding that has been losslessly removed. The seed for the padding algorithm is taken from the first sector of the image.

### PS3 Encoding

PS3 images normally have sections of the image that are encrypted with a disc key not present on the image but derived from data in a special area on the disc. This special area is not normally readable by PC Bluray drives and instead requires reading with a PS3 drive on a console with hacked firmware. This is what 3k3y does to generate IRD files, so an IRD file for each image is required when handling PS3 images. The common PS3 key and iv are also required on encode to generate the disc key from the contents of the IRD file.

With this in mind, the encoding for a PS3 image is:
* The disc key required to re-encrypt decrypted sectors when decoding
* The IRD file used on encode to generate the disc key. By storing the IRD file in the archive it can maintain filename parity with the image it's associated with

### Wii Encoding

Encoding for a Wii image is a 32 bit LSB integer defining how many partitions are modelled, a partition entry for each modelled partition, followed by a bit array defining the padding modelling status of the remaining sectors (similar to the GameCube padding encoding but only for the sectors not already modelled by the partition modelling).

#### Wii Partition Entry

A partition entry is made up of the 16 byte title key, a 32 bit LSB integer defining the sector index of the first group in the partition, a 32 bit LSB integer defining the number of full groups in the partition, followed by a bit array defining which groups have been modelled. A modelled group is stored decrypted with SHA1 chunks zeroed in USB2048, an unmodelled group is stored as-is in USB2048.

## Other useful information

* Filenames are not stored in the .oar archive, instead they are stored in the accompanying .tit file in the order they are present in the archive. This allows for the image names to be updated without having to touch the archive
* The files are deterministically ordered by filesize, then by memcmp in the case of identical filesize. Doing it and in this way has some key benefits:
  * Ordering is normally very quick, most images have differing filesize and data tracks tend to diverge quickly
  * Similar filesizes are grouped together. The archive does sector-level deduplication which tends to work well for data tracks but doesn't work well for misaligned audio tracks (different print runs with potentially different write offsets mean that otherwise matching audio tracks might not match). Grouping similar filesizes together allows misaligned audio tracks to be efficiently compressed with a standard compressor
 
