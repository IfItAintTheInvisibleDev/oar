CC=gcc
CFLAGS= -O3 -Wall -D_FILE_OFFSET_BITS=64 -pedantic
DEPS =
OBJ = common.o cryptowrap.o decode.o encode.o filetype.o filetypebin.o filetypeiso.o filetypengc.o filetypeps3.o filetypewii.o headers.o nod.o whereami.o ybe.o
HEAD = common.c cryptowrap.c decode.c encode.c oar.c filetype.c filetypebin.c filetypeiso.c filetypengc.c filetypeps3.c filetypewii.c headers.c nod.c ybe.c

.PHONY : all
all : oar oar-mbedtls oar.exe oarfs

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) --std=c89 -lcrypto -Izlib

oar: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) --std=c89 -lcrypto -Izlib zlib/inflate.c zlib/inftrees.c zlib/inffast.c zlib/zutil.c zlib/adler32.c zlib/crc32.c oar.c

oarfs: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) oarfs.c -lcrypto -Izlib zlib/inflate.c zlib/inftrees.c zlib/inffast.c zlib/zutil.c zlib/adler32.c zlib/crc32.c `pkg-config fuse --cflags --libs`

oar.exe: $(OBJ)
	i686-w64-mingw32-gcc -o oar.exe -O3 -Wall -Wno-format -D_FILE_OFFSET_BITS=64 -pedantic common.c cryptowrap.c decode.c encode.c oar.c filetype.c filetypebin.c filetypeiso.c filetypengc.c filetypeps3.c filetypewii.c headers.c nod.c ybe.c -DMBEDTLS_LIB -I mbedtls/include mbedtls/library/sha1.c mbedtls/library/sha256.c mbedtls/library/aes.c mbedtls/library/aesni.c mbedtls/library/padlock.c -I zlib zlib/inflate.c zlib/inftrees.c zlib/inffast.c zlib/zutil.c zlib/adler32.c zlib/crc32.c -I whereami whereami/whereami.c

oar-arm: $(OBJ)
	arm-linux-gnueabi-gcc -o oar-arm -static -O3 -Wall -Wno-format -D_FILE_OFFSET_BITS=64 -pedantic common.c cryptowrap.c decode.c encode.c oar.c filetype.c filetypebin.c filetypeiso.c filetypengc.c filetypeps3.c filetypewii.c headers.c nod.c ybe.c -DMBEDTLS_LIB -I mbedtls/include mbedtls/library/sha1.c mbedtls/library/sha256.c mbedtls/library/aes.c mbedtls/library/aesni.c mbedtls/library/padlock.c -I zlib zlib/inflate.c zlib/inftrees.c zlib/inffast.c zlib/zutil.c zlib/adler32.c zlib/crc32.c -I whereami whereami/whereami.c

whereami.o:
	$(CC) -c -o $@ $< $(CFLAGS) -Iwhereami whereami/whereami.c

oar-mbedtls: $(HEAD) whereami.o
	$(CC) -o $@ $^ $(CFLAGS) -DMBEDTLS_LIB -Imbedtls/include mbedtls/library/sha1.c mbedtls/library/sha256.c mbedtls/library/aes.c mbedtls/library/aesni.c mbedtls/library/padlock.c -lz

genbin:
	$(CC) -ogenbin cryptowrap.c cryptowrap.h filetype.h ybe.h ybe.c common.c common.h genbin.c --std=c89 -lcrypto

.PHONY: clean

clean:
	rm *.o genbin oar oar-mbedtls oar.exe oar-arm oarfs
