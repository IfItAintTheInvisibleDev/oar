# oar

Optical ARchiver, archives related disc images into a de-duplicated format.

## Features

* De-duplicate sectors at the data level, after any modelling has been applied
* Models BIN (raw/uncooked files with 2352 bytes per sector) files (losslessly removes ECC and other non-data elements)
* Special handling of GameCube, Wii and PS3 images to model padding and encryption
* Integrity checks built into the format
* Light on RAM usage. On encode reads bin input images once to build a de-duplication map then again to obtain the unique sectors
* Binarily deterministic. Archiving the same input images but in a different order or with different filenames still yields the same archive
  * The input is ordered by hash before being archived
  * Filenames are separated into their own file, the oar archive can remain unaltered until the user wants to add/remove an image to the set

## Usage

Archive all images in some_dir into some_dir.oar
```
oar some_dir
```

Archive all images in some_dir into some_archive.oar
```
oar some_dir --outfile some_archive.oar
```

Extract images contained in archive.oar to the current directory
```
oar archive.oar
```

Perform an integrity check on archive.oar using SHA256 hashes stored in the footer and basic sanity checks
```
oar archive.oar --integrity
```

Perform a full decode of archive.oar but don't write the images to file
```
oar archive.oar --test
```

(Linux only) Mount an archive to access the contained images without having to extract them
```
oarfs archive mountpoint
```

## Memory Requirements

### Encode

Encode memory requirements scale with total sector count of the input and also depend on the filetype of the images and how duplicated the sectors are. The following numbers indicate the worst case:

Type | sectortable | DLT | enctype
--- | --- | ---
BIN | 16 bytes/sector | 4 bytes/sector | 2 bits/sector
ISO | 16 bytes/sector | 4 bytes/sector | 0
NGC | 16 bytes/sector | 4 bytes/sector | 1 bit/sector
PS3 | 16 bytes/sector | 4 bytes/sector | 0
WII | 16 bytes/sector | 4 bytes/sector | 1 bit/sector

At worst every ~95 GiB of input requires ~1 GiB of concurrent memory to encode. In practice this is usually much lower.

* sectortable is a table containing hashes and indexes of all unique sectors in the archive. Every duplicate sector found removes an entry from the sectortable, 16 bytes
* DLT maps every sector to its data location in the USB's
* Encoding requires two passes over the input, enctype stores the barebones encoding information required to avoid redoing work in the second pass

### Decode

Decode memory requirements scale with the size of the largest image stored in the archive and also depend on the filetype of the image:

Type | Largest image | Memory Required
--- | --- | ---
BIN file | 1 GiB | Up to 129 MiB (depending on how well YBE can model the raw sectors)
ISO file | 4.7 GiB | 10 MiB
ISO file | 8.5 GiB | 17 MiB
ISO file | 25 GiB | 50 MiB
ISO file | 50 GiB | 100 MiB
ISO file | 100 GiB | 200 MiB
ISO file | 128 GiB | 256 MiB

Note these tables only represent the scaling memory requirement, there is also some small overhead used as working memory

## Compile instructions

### From Linux

* "make" to compile everything (oar, oar-mbedtls, oar.exe and oarfs), or "make x" to compile x
* oar is the commandline encoder/decoder with OpenSSL backend
* oar-mbedtls is the commandline encoder/decoder with mbedtls backend
* oar-mingw.exe is the commandline encoder/decoder cross-compiled for Windows using MingW
* oarfs is a FUSE implementation for mounting an archive to the filesystem
* test.sh compiles the commandline binaries and performs automated testing using randomly seeded test images. The windows binary is tested using wine

### From Windows

* It should be pretty simple to compile the windows commandline binary with MingW. The Linux makefile cross-compiles a windows binary with MingW, adapt the i686-w64-mingw32-gcc entry from the makefile to use your MingW binary

## Archive Format

See FORMAT.md

## Thanks

The error code modelling for BIN images has been adapted from Neill Corlett's ECM code.

## License

GPLv3

