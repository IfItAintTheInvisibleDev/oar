/**
 * @file Implement basic common functions
 */

#include "common.h"
#include "filetypebin.h"
#include "ybe.h"

#include <assert.h>

void bin2ascii(uint8_t *bin, char *ascii, int binlen)
{
	int i=0;
	while(i<binlen*2)
	{
		if( ((bin[i/2]>>(i%2?0:4))%16)<10 )
			ascii[i]=48+((bin[i/2]>>(i%2?0:4))%16);
		else
			ascii[i]=55+((bin[i/2]>>(i%2?0:4))%16);
		++i;
	}
	ascii[i]=0;
}

void readmap(size_t size, off_t offset, int secsize, uint64_t *start, uint64_t *end, uint64_t *trimstart, uint64_t *trimend)
{
	uint64_t chkoffset, chksize;
	start[0]=offset/secsize;/* index of first touched sector, partial or otherwise */
	trimstart[0]=offset%secsize;/* Number of bytes to trim from first sector */
	end[0]=(offset+size)/secsize;/* index one beyond last touched sector */
	trimend[0]=secsize-((offset+size)%secsize);/* Number of bytes to trim from last sector */
	if(trimend[0]==secsize)
		trimend[0]=0;/* No partial end sector */
	else
		++end[0];/* Partial end sector, need to read an extra sector to get it */

	chkoffset=(start[0]*secsize)+trimstart[0];
	chksize=(end[0]*secsize)-trimend[0];
	chksize-=chkoffset;

	assert(chkoffset==offset);
	assert(chksize==size);
}

int compare_file(const void *a, const void *b)
{
	char *scr=0;
	int i=0, size=0;
	uint64_t sa, sb;
	FILE *fa=0, *fb=0;
	struct file_header *af=((struct file_header *)a);
	struct file_header *bf=((struct file_header *)b);

	sa=af->sector_count;
	sa*=af->type==FILE_TYPE_BIN?2352:2048;
	sb=bf->sector_count;
	sb*=bf->type==FILE_TYPE_BIN?2352:2048;

	if(sa==sb)
	{
		/* Filesize identical, memcmp to determine order */

		size = af->type==FILE_TYPE_BIN?2352:2048;
		scr=mallocount(2*size, NULL, NULL);
		fa=fopen(af->filename, "rb");
		fb=fopen(bf->filename, "rb");
		i=0;
		while(i<af->sector_count)
		{
			freadass(scr, 1, size, fa);
			freadass(scr+size, 1, size, fb);
			if(memcmp(scr, scr+size, size)!=0)
			{
				size=memcmp(scr, scr+size, size);
				free(scr);
				fclose(fa);
				fclose(fb);
				return size;
			}
			++i;
		}
		free(scr);
		fclose(fa);
		fclose(fb);
		return 0;
	}
	else
		return sa>sb?-1:1;
}

int compare_sector(const void *a, const void *b)
{
	int ret, aid, bid;
	ret = memcmp( ((struct sector *)a)->hash, ((struct sector *)b)->hash, 12);
	if(ret==0)
	{
		aid = ((struct sector *)a)->id;
		bid = ((struct sector *)b)->id;
		return aid==bid?0:(aid>bid?1:-1);
	}
	else
		return ret;
}

int compare_sector_hash(const void *a, const void *b)
{
	return memcmp( ((struct sector *)a)->hash, ((struct sector *)b)->hash, 12);
}

uint32_t get32lsb(const uint8_t* src) {
	return
		(((uint32_t)(src[0])) <<  0) |
		(((uint32_t)(src[1])) <<  8) |
		(((uint32_t)(src[2])) << 16) |
		(((uint32_t)(src[3])) << 24);
}
uint32_t get32msb(const uint8_t* src) {
	return
		(((uint32_t)(src[3])) <<  0) |
		(((uint32_t)(src[2])) <<  8) |
		(((uint32_t)(src[1])) << 16) |
		(((uint32_t)(src[0])) << 24);
}
uint64_t get64lsb(const uint8_t* src)
{
	return
	(((uint64_t)(src[0])) <<  0) |
	(((uint64_t)(src[1])) <<  8) |
	(((uint64_t)(src[2])) << 16) |
	(((uint64_t)(src[3])) << 24) |
	(((uint64_t)(src[4])) << 32) |
	(((uint64_t)(src[5])) << 40) |
	(((uint64_t)(src[6])) << 48) |
	(((uint64_t)(src[7])) << 56);
}
uint64_t get64msb(const uint8_t* src)
{
	return
	(((uint64_t)(src[7])) <<  0) |
	(((uint64_t)(src[6])) <<  8) |
	(((uint64_t)(src[5])) << 16) |
	(((uint64_t)(src[4])) << 24) |
	(((uint64_t)(src[3])) << 32) |
	(((uint64_t)(src[2])) << 40) |
	(((uint64_t)(src[1])) << 48) |
	(((uint64_t)(src[0])) << 56);
}
void put32lsb(uint8_t* dest, uint32_t value) {
	dest[0] = (uint8_t)(value      );
	dest[1] = (uint8_t)(value >>  8);
	dest[2] = (uint8_t)(value >> 16);
	dest[3] = (uint8_t)(value >> 24);
}
void put32msb(uint8_t* dest, uint32_t value) {
	dest[3] = (uint8_t)(value      );
	dest[2] = (uint8_t)(value >>  8);
	dest[1] = (uint8_t)(value >> 16);
	dest[0] = (uint8_t)(value >> 24);
}
void put64lsb(uint8_t* dest, uint64_t value)
{
	dest[0] = (uint8_t)(value      );
	dest[1] = (uint8_t)(value >>  8);
	dest[2] = (uint8_t)(value >> 16);
	dest[3] = (uint8_t)(value >> 24);
	dest[4] = (uint8_t)(value >> 32);
	dest[5] = (uint8_t)(value >> 40);
	dest[6] = (uint8_t)(value >> 48);
	dest[7] = (uint8_t)(value >> 56);
}
void put64msb(uint8_t* dest, uint64_t value)
{
	dest[7] = (uint8_t)(value      );
	dest[6] = (uint8_t)(value >>  8);
	dest[5] = (uint8_t)(value >> 16);
	dest[4] = (uint8_t)(value >> 24);
	dest[3] = (uint8_t)(value >> 32);
	dest[2] = (uint8_t)(value >> 40);
	dest[1] = (uint8_t)(value >> 48);
	dest[0] = (uint8_t)(value >> 56);
}

void mempeak_update(uint64_t *curr, uint64_t *peak, uint64_t new)
{
	if(curr[0]+new>peak[0])
		peak[0]=curr[0]+new;
}

FILE *fopenass(const char *pathname, const char *mode)
{
	FILE *ret;
	ret=fopen(pathname, mode);
	assert(ret!=NULL);
	return ret;
}

size_t freadass(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	size_t io;
	io=fread(ptr, size, nmemb, stream);
	assert(io==(size*nmemb));
	return io;
}

int fseekass(FILE *stream, long offset, int whence)
{
	int ret;
	ret=fseek(stream, offset, whence);
	assert(ret!=-1);
	return ret;
}

long ftellass(FILE *stream)
{
	long ret;
	ret=ftell(stream);
	assert(ret!=-1);
	return ret;
}

size_t fwriteass(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	size_t io;
	io=fwrite(ptr, size, nmemb, stream);
	assert(io==(size*nmemb));
	return io;
}

void* mallocount(size_t size, uint64_t *memcurr, uint64_t *mempeak)
{
	void *ret=0;
	ret=malloc(size);
	assert(ret!=NULL);
	memset(ret, 0, size);
	if(memcurr&&mempeak)
	{
		memcurr[0]+=size;
		mempeak_update(memcurr, mempeak, 0);
	}
	return ret;
}

void *reallocount(void *ptr, size_t size, uint64_t *memcurr, uint64_t *mempeak, size_t oldsize)
{
	void *ret=0;
	ret=realloc(ptr, size);
	if(size>0)
		assert(ret!=NULL);
	if(memcurr&&mempeak)
	{
		memcurr[0]-=oldsize;
		memcurr[0]+=size;
		mempeak_update(memcurr, mempeak, 0);
	}
	return ret;
}

int oar_abort(char *msg)
{
	if(msg)
		printf("%s", msg);
	return 0;
}

int sectorsize_to_index(int datasize)
{
	switch(datasize)
	{
		case 2048:
			return 0;

		case 2324:
			return 1;

		case 2336:
			return 2;

		case 2352:
			return 3;
	}
	printf("Warning: sectorsize_to_index() has been fed an invalid datasize, this can't end well\n");
	return -1;/* As the return value is mostly used as an array index a negative value should fail fast */
}

int updatedlt(uint8_t *dlt, struct sector *keysector, struct sector *sectortable, uint32_t sectortable_len, uint32_t bucketindex)
{
	struct sector *tmpsector=0;

	tmpsector = bsearch(keysector, sectortable, sectortable_len, sizeof(struct sector), compare_sector_hash);
	assert(tmpsector!=NULL);

	if(tmpsector->id == keysector->id)
	{
		/* first time encountering sector */
		put32lsb(dlt+4*keysector->id, bucketindex);
		return 1;
	}
	else
	{
		/* Dupe sector, use the location of the unique sector */
		memcpy(dlt+4*keysector->id, dlt+4*tmpsector->id, 4);
		return 0;
	}
}

void dataraw(uint64_t index, uint8_t *dlt, FILE *fin, uint64_t usbsize, uint64_t usbloc, uint8_t *buffer)
{
	uint64_t seek;
	/* Copy data from bucket */
	seek=usbsize;
	seek*=get32lsb(dlt+4*index);
	seek+=usbloc;
	fseekass(fin, seek, SEEK_SET);
	/* Read from bucket */
	read_hash_write(fin, NULL, NULL, NULL, buffer, usbsize);
}

void update_buffer(char *buffer, int *bufferloc, uint64_t index, uint64_t start, uint64_t end, uint64_t trimstart, uint64_t trimend, int secsize, uint8_t *sector)
{
	uint64_t windowstart, windowend;

	windowstart= (index==start?trimstart:0);
	windowend= (index==(end-1)?secsize-trimend:secsize);

	memcpy(buffer+bufferloc[0], sector+windowstart, windowend-windowstart);
	bufferloc[0]+=(windowend-windowstart);
}

/* Load an images DLT and ENC from file */
void load_image(struct file_header *fh, uint8_t **dltptr, uint8_t **encptr, FILE *fin)
{
	assert(fh!=NULL);
	assert(dltptr!=NULL);
	assert(encptr!=NULL);
	assert(fin!=NULL);

	dltptr[0]=reallocount(dltptr[0], 4*fh->sector_count, NULL, NULL, 0);
	fseekass(fin, fh->dltloc, SEEK_SET);
	freadass(dltptr[0], 1, 4*fh->sector_count, fin);

	encptr[0]=reallocount(encptr[0], fh->enc_len, NULL, NULL, 0);
	fseekass(fin, fh->encloc, SEEK_SET);
	freadass(encptr[0], 1, fh->enc_len, fin);
}
