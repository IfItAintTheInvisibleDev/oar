/**
 * @file Define basic common functions
 */

#ifndef OAR_COMMON
#define OAR_COMMON

#define FILE_TYPE_UNDEFINED 0
#define SECTORBUFFERSIZE (2048*2352*4)

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>

#include "headers.h"

struct stats
{
	uint64_t memret;
	uint64_t memcurr;
	uint64_t mempeak;
	int64_t running_bytes_decrypted_by_modelling;
	int64_t running_bytes_saved_by_dedupe;
	int64_t running_bytes_saved_by_modelling;
};

void bin2ascii(uint8_t *bin, char *ascii, int binlen);

/* Convert a read range into the sectors that cover it and what needs to be trimmed from either end */
void readmap(size_t size, off_t offset, int secsize, uint64_t *start, uint64_t *end, uint64_t *trimstart, uint64_t *trimend);

/**
 * Compare two image files for ordering
 *
 * SHA1 ordering is simple but requires a full read of all images to determine
 * order. This function orders by filesize, secondarily with memcmp if required.
 *
 * @param a struct file_header *a
 * @param b struct file_header *b
 */
int compare_file(const void *a, const void *b);

/**
 * Fully compare two sector structs
 *
 * @param a struct sector *a
 * @param b struct sector *b
 */
int compare_sector(const void *a, const void *b);

/**
 * Compare sector structs by hash only
 *
 * @param a struct sector *a
 * @param b struct sector *b
 */
int compare_sector_hash(const void *a, const void *b);

/* Portably read and write various integer types */
uint32_t get32lsb(const uint8_t* src);
uint32_t get32msb(const uint8_t* src);
uint64_t get64lsb(const uint8_t* src);
uint64_t get64msb(const uint8_t* src);
void put32lsb(uint8_t* dest, uint32_t value);
void put32msb(uint8_t* dest, uint32_t value);
void put64lsb(uint8_t* dest, uint64_t value);
void put64msb(uint8_t* dest, uint64_t value);

void mempeak_update(uint64_t *curr, uint64_t *peak, uint64_t new);

FILE *fopenass(const char *pathname, const char *mode);
/** fread and check result */
size_t freadass(void *ptr, size_t size, size_t nmemb, FILE *stream);
int fseekass(FILE *stream, long offset, int whence);
long ftellass(FILE *stream);
/** fwrite and check result */
size_t fwriteass(const void *ptr, size_t size, size_t nmemb, FILE *stream);
/** malloc, assert, zero and keep track of memory usage */
void* mallocount(size_t size, uint64_t *memcurr, uint64_t *mempeak);
/** realloc, assert and keep track of memory usage */
void *reallocount(void *ptr, size_t size, uint64_t *memcurr, uint64_t *mempeak, size_t oldsize);

/**
 * Print a message and return 0
 *
 * @param msg The message to print
 */
int oar_abort(char *msg);

/* Convert from sector data length to index in bucket arrays */
int sectorsize_to_index(int datasize);

/* Update dlt with the keysector */
/* Returns 1 for unique sector found, 0 for duplicate sector */
int updatedlt(uint8_t *dlt, struct sector *keysector, struct sector *sectortable, uint32_t sectortable_len, uint32_t bucketindex);

/* Read raw sector data contained in bucket pointed to by dlt[index] into buffer */
void dataraw(uint64_t index, uint8_t *dlt, FILE *fin, uint64_t usbsize, uint64_t usbloc, uint8_t *buffer);

/* Update a read buffer with new data if in range */
void update_buffer(char *buffer, int *bufferloc, uint64_t index, uint64_t start, uint64_t end, uint64_t trimstart, uint64_t trimend, int secsize, uint8_t *sector);

/* Load an images DLT and ENC from file */
void load_image(struct file_header *fh, uint8_t **dltptr, uint8_t **encptr, FILE *fin);

#endif
