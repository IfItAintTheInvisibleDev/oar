#include "cryptowrap.h"
#include "common.h"


/* Implement crypto wrapper */
#ifdef MBEDTLS_LIB
void mbedtls_platform_zeroize(void *buf, size_t len)
{
	memset(buf, 0, len);
}
void oar_sha1_init(oar_sha1_ctx *ctx)
{
	int ret;
	mbedtls_sha1_init(ctx);
	ret=mbedtls_sha1_starts_ret(ctx);
	assert(ret==0);
}
void oar_sha1_update(oar_sha1_ctx *ctx, const void *data, size_t len)
{
	int ret;
	ret=mbedtls_sha1_update_ret(ctx, data, len);
	assert(ret==0);
}
void oar_sha1_final(unsigned char *md, oar_sha1_ctx *ctx)
{
	int ret;
	ret=mbedtls_sha1_finish_ret(ctx, md);
	assert(ret==0);
}
void oar_sha1(const unsigned char *d, size_t n, unsigned char *md)
{
	mbedtls_sha1_ret(d, n, md);
}
void oar_sha256_init(oar_sha256_ctx *ctx)
{
	int ret;
	mbedtls_sha256_init(ctx);
	ret=mbedtls_sha256_starts_ret(ctx, 0);
	assert(ret==0);
}
void oar_sha256_update(oar_sha256_ctx *ctx, const void *data, size_t len)
{
	int ret;
	ret=mbedtls_sha256_update_ret(ctx, data, len);
	assert(ret==0);
}
void oar_sha256_final(unsigned char *md, oar_sha256_ctx *ctx)
{
	int ret;
	ret=mbedtls_sha256_finish_ret(ctx, md);
	assert(ret==0);
}
void oar_sha256(const unsigned char *d, size_t n, unsigned char *md)
{
	mbedtls_sha256_ret(d, n, md, 0);
}

void oar_aes128cbc_decrypt(uint8_t *key, uint8_t *iv, uint8_t *ctext, uint8_t *ptext, uint64_t size)
{
	uint8_t keyuse[16];
	uint8_t ivuse[16];
	mbedtls_aes_context aes;
	int ret;

	memcpy(keyuse, key, 16);
	memcpy(ivuse, iv, 16);

	mbedtls_aes_init(&aes);
	ret=mbedtls_aes_setkey_dec(&aes, keyuse, 128);
	assert(ret==0);
	ret=mbedtls_aes_crypt_cbc(&aes, MBEDTLS_AES_DECRYPT, size, ivuse, ctext, ptext);
	assert(ret==0);
}

void oar_aes128cbc_encrypt(uint8_t *key, uint8_t *iv, uint8_t *ctext, uint8_t *ptext, uint64_t size)
{
	uint8_t keyuse[16];
	uint8_t ivuse[16];
	mbedtls_aes_context aes;
	int ret;

	memcpy(keyuse, key, 16);
	memcpy(ivuse, iv, 16);

	mbedtls_aes_init(&aes);
	ret=mbedtls_aes_setkey_enc(&aes, keyuse, 128);
	assert(ret==0);
	ret=mbedtls_aes_crypt_cbc(&aes, MBEDTLS_AES_ENCRYPT, size, ivuse, ptext, ctext);
	assert(ret==0);
}

#else

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/sha.h>
void oar_sha1_init(oar_sha1_ctx *ctx)
{
	int ret;
	ret=SHA1_Init(ctx);
	assert(ret==1);
}
void oar_sha1_update(oar_sha1_ctx *ctx, const void *data, size_t len)
{
	int ret;
	ret=SHA1_Update(ctx, data, len);
	assert(ret==1);
}
void oar_sha1_final(unsigned char *md, oar_sha1_ctx *ctx)
{
	int ret;
	ret=SHA1_Final(md, ctx);
	assert(ret==1);
}
void oar_sha1(const unsigned char *d, size_t n, unsigned char *md)
{
	SHA1(d, n, md);
}
void oar_sha256_init(oar_sha256_ctx *ctx)
{
	int ret;
	ret=SHA256_Init(ctx);
	assert(ret==1);
}
void oar_sha256_update(oar_sha256_ctx *ctx, const void *data, size_t len)
{
	int ret;
	ret=SHA256_Update(ctx, data, len);
	assert(ret==1);
}
void oar_sha256_final(unsigned char *md, oar_sha256_ctx *ctx)
{
	int ret;
	ret=SHA256_Final(md, ctx);
	assert(ret==1);
}
void oar_sha256(const unsigned char *d, size_t n, unsigned char *md)
{
	SHA256(d, n, md);
}

void oar_aes128cbc_decrypt(uint8_t *key, uint8_t *iv, uint8_t *ctext, uint8_t *ptext, uint64_t size)
{
	EVP_CIPHER_CTX *ctx;
	int len, ret;

	/* Create and initialise the context */
	ctx=EVP_CIPHER_CTX_new();
	assert(ctx!=NULL);

	/*
	 * Initialise the encryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits
	 */
	ret=EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv);
	assert(ret==1);

	/* Disable padding, we should be dealing with multiples of 16 bytes anyway and we don;t want a full padding block created */
	EVP_CIPHER_CTX_set_padding(ctx, 0);

	/*
	 * Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	ret=EVP_DecryptUpdate(ctx, ptext, &len, ctext, size);
	assert(ret==1);

	/*
	 * Finalise the encryption. Further ciphertext bytes may be written at
	 * this stage.
	 */
	ret=EVP_DecryptFinal_ex(ctx, ptext + len, &len);
	assert(ret==1);

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);
}

void oar_aes128cbc_encrypt(uint8_t *key, uint8_t *iv, uint8_t *ctext, uint8_t *ptext, uint64_t size)
{
	EVP_CIPHER_CTX *ctx;
	int len, ret;

	/* Create and initialise the context */
	ctx=EVP_CIPHER_CTX_new();
	assert(ctx!=NULL);

	/*
	 * Initialise the encryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits
	 */
	ret=EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv);
	assert(ret==1);

	/* Disable padding, we should be dealing with multiples of 16 bytes anyway and we don;t want a full padding block created */
	EVP_CIPHER_CTX_set_padding(ctx, 0);

	/*
	 * Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	ret=EVP_EncryptUpdate(ctx, ctext, &len, ptext, size);
	assert(ret==1);

	/*
	 * Finalise the encryption. Further ciphertext bytes may be written at
	 * this stage.
	 */
	ret=EVP_EncryptFinal_ex(ctx, ctext + len, &len);
	assert(ret==1);

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);
}
#endif

void read_hash_write(FILE *fin, FILE *fout, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256, void *data, uint64_t size)
{
	if(fin)
		freadass(data, 1, size, fin);
	if(sha1)
		oar_sha1_update(sha1, data, size);
	if(sha256)
		oar_sha256_update(sha256, data, size);
	if(fout)
		fwriteass(data, 1, size, fout);
}

