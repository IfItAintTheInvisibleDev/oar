#ifndef OAR_CRYPTOWRAP
#define OAR_CRYPTOWRAP

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

/* Support OpenSSL and MBEDTLS crypto libraries with a wrapper */
/* Because cross-compilation of OpenSSL from Linux to Windows with MingW is not fun */
#ifdef MBEDTLS_LIB
	#include <mbedtls/aes.h>
	#include <mbedtls/sha1.h>
	#include <mbedtls/sha256.h>
	typedef mbedtls_sha1_context oar_sha1_ctx;
	typedef mbedtls_sha256_context oar_sha256_ctx;
	void mbedtls_platform_zeroize(void *buf, size_t len);
#else
	#include <openssl/conf.h>
	#include <openssl/evp.h>
	#include <openssl/err.h>
	#include <openssl/sha.h>
	typedef SHA_CTX oar_sha1_ctx;
	typedef SHA256_CTX oar_sha256_ctx;
#endif

void oar_sha1_init(oar_sha1_ctx *ctx);
void oar_sha1_update(oar_sha1_ctx *ctx, const void *data, size_t len);
void oar_sha1_final(unsigned char *md, oar_sha1_ctx *ctx);
void oar_sha1(const unsigned char *d, size_t n, unsigned char *md);

void oar_sha256_init(oar_sha256_ctx *ctx);
void oar_sha256_update(oar_sha256_ctx *ctx, const void *data, size_t len);
void oar_sha256_final(unsigned char *md, oar_sha256_ctx *ctx);
void oar_sha256(const unsigned char *d, size_t n, unsigned char *md);

void oar_aes128cbc_decrypt(uint8_t *key, uint8_t *iv, uint8_t *ctext, uint8_t *ptext, uint64_t len);
void oar_aes128cbc_encrypt(uint8_t *key, uint8_t *iv, uint8_t *ctext, uint8_t *ptext, uint64_t len);

/* Condense read/hash/write code with this helper function
 * If fin != NULL read size bytes into data
 * If sha1 != NULL update sha1 with size bytes from data
 * If sha256 != NULL update sha256 with size bytes from data
 * If fout !=NULL write size bytes from data
 */
void read_hash_write(FILE *fin, FILE *fout, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256, void *data, uint64_t size);

#ifdef __cplusplus
}
#endif

#endif
