#include "decode.h"
#include "common.h"

int dodecode(int *indexes, int indexes_count, int thisindex)
{
	int i=0;
	if(indexes==NULL)
		return 1;
	while(i<indexes_count)
	{
		if(indexes[i]==thisindex)
			return 1;
		++i;
	}
	return 0;
}

int decode(struct oar_header *oar, struct oar_options *ops, char *infile)
{
	char *str_builder=0;
	uint8_t *enc_data=0, *scratch=0;
	uint8_t *dlt=0;
	uint64_t i=0;
	FILE *fin=0, *fout=0, *fancillary=0, *titin=0;
	oar_sha1_ctx sha1;
	struct filetype_operations *fops=0;
	struct stats *stats=0;

	stats=mallocount(sizeof(struct stats), NULL, NULL);
	stats->memcurr=1024;

	fops=mallocount(FILE_TYPE_OP_ARRAY_SIZE*sizeof(struct filetype_operations), &stats->memcurr, &stats->mempeak);
	populate_filetype_ops(fops);

	stats=mallocount(sizeof(struct stats), &stats->memcurr, &stats->mempeak);
	scratch=mallocount(2352, &stats->memcurr, &stats->mempeak);
	str_builder=mallocount(FILENAME_STRING_LEN, &stats->memcurr, &stats->mempeak);

	if(ops->test)
		printf("> Test '%s.oar'\n", infile);
	else
		printf("> Decode '%s.oar' into current directory\n", infile);

	if(ops->verbose)
		printf("Load archive\n");

	/* Read header from file */
	assert(strlen(infile)+5<FILENAME_STRING_LEN);
	sprintf(str_builder, "%s.oar", infile);
	fin=fopenass(str_builder, "rb");

	sprintf(str_builder, "%s.tit", infile);
	titin=fopen(str_builder, "rb");

	load_archive(oar, fin, titin, NULL);
	if(titin)
		fclose(titin);

	if(oar->version>oar->program_version)
	{
		return oar_abort("Warning, archive was created with a newer version flag than this oar binary supports. A version bump could indicate a breaking change to the format like a bug fix or use of a new feature. Aborting decode. Update your binary.\n");
	}

	/* Decode every image */
	i=0;
	while(i<oar->filecount)
	{
		if(dodecode(ops->indexes, ops->indexes_count, i))
		{
			load_image(&oar->file_headers[i], &dlt, &enc_data, fin);
			stats->memcurr+=oar->file_headers[i].enc_len;
			stats->memcurr+=(4*oar->file_headers[i].sector_count);

			oar_sha1_init(&sha1);
			if(!ops->test)/* Open image for writing if we are not performing a test */
				fout=fopenass(oar->file_headers[i].filename, "wb");

			printf(" Decode '%s'\n", oar->file_headers[i].filename);
			fops[oar->file_headers[i].type].decode(&oar->file_headers[i], stats, fin, fout, &sha1, dlt, oar->usbloc, enc_data);

			/* Special handling not encapsulated by decode(). Avoid implementing special handling here where possible as it's messy */
			switch(oar->file_headers[i].type)
			{
				case FILE_TYPE_PS3:/* Write accompanying IRD file from ENC if it's not already present */
					/* Check for existence of IRD file */
					strcpy(str_builder, oar->file_headers[i].filename);
					assert(strlen(str_builder)>4);
					memcpy(str_builder+strlen(str_builder)-3, "ird", 3);
					fancillary=fopen(str_builder, "rb");
					if(fancillary==NULL)
					{
						/* IRD not present, write it from enc */
						fancillary=fopenass(str_builder, "wb");
						fwriteass(enc_data+16, 1, oar->file_headers[i].enc_len-16, fancillary);
						fclose(fancillary);
					}
					break;
			}
			mempeak_update(&stats->memcurr, &stats->mempeak, stats->memret);

			if(!ops->test)/* If we are performing a test there's no output file to close */
				fclose(fout);
			oar_sha1_final(scratch, &sha1);

			if(memcmp(scratch, oar->file_headers[i].sha1, 20)!=0)
				return oar_abort("Failed, SHA1 hash mismatch, abort\n");

			stats->memcurr-=oar->file_headers[i].enc_len;
			stats->memcurr-=(4*oar->file_headers[i].sector_count);
		}

		++i;
	}

	printf("Peak memory usage: ~%"PRIu64" MiB\n", ((stats->mempeak/1048576)+1));
	printf("Success\n");

	/* Free any memory malloc'd in this function */
	if(enc_data)
		free(enc_data);
	free(fops);
	free(scratch);
	free(stats);
	free(str_builder);
	return 1;
}

int integrity(struct oar_header *oar, struct oar_options *ops, char* infile)
{
	char *str_builder=0;
	int64_t endloc=0;
	uint8_t *scratch=0, *tmp=0;
	uint64_t i=0, j=0;
	uint64_t bucketsize[4]={2048, 2324, 2336, 2352};
	FILE *fin=0, *titin=0;
	oar_sha256_ctx sha256;

	scratch=mallocount(OAR_SCRATCH_LEN, NULL, NULL);
	str_builder=mallocount(FILENAME_STRING_LEN, NULL, NULL);

	printf("> Integrity check '%s'\n", infile);

	oar_sha256_init(&sha256);

	/* Load archive */
	printf("Load archive\n");
	assert(strlen(infile)+5<FILENAME_STRING_LEN);
	sprintf(str_builder, "%s.oar", infile);
	fin=fopenass(str_builder, "rb");

	sprintf(str_builder, "%s.tit", infile);
	titin=fopen(str_builder, "rb");
	if(titin==NULL)
		printf("Warning: Failed to open title file, continuing integrity check of archive\n");

	load_archive(oar, fin, titin, &sha256);
	if(titin!=NULL)
		fclose(titin);

	/* Size checks */
	printf("Size checks\n");
	fseekass(fin, 0, SEEK_END);
	endloc=ftellass(fin);
	assert(oar->oar_filesize==oar->shaloc);
	assert(endloc==oar->shaloc+OAR_FOOTER_LEN);

	/* Read footer */
	printf("Read footer\n");
	fseekass(fin, oar->shaloc, SEEK_SET);
	freadass(oar->sha256, 1, OAR_FOOTER_LEN, fin);
	/* HED hash check */
	printf("HED hash check\n");
	oar_sha256_final(scratch, &sha256);
	assert(memcmp(scratch, oar->sha256+(0*32), 32)==0);

	/* DLT hash check */
	printf("DLT hash check\n");
	oar_sha256_init(&sha256);
	i=0;
	while(i<oar->filecount)
	{
		tmp=reallocount(tmp, 4*oar->file_headers[i].sector_count, NULL, NULL, 0);
		fseekass(fin, oar->file_headers[i].dltloc, SEEK_SET);
		read_hash_write(fin, NULL, NULL, &sha256, tmp, 4*oar->file_headers[i].sector_count);

		++i;
	}
	oar_sha256_final(scratch, &sha256);
	assert(memcmp(scratch, oar->sha256+(1*32), 32)==0);

	/* ENC hash check */
	printf("ENC hash check\n");
	oar_sha256_init(&sha256);
	i=0;
	while(i<oar->filecount)
	{
		tmp=reallocount(tmp, oar->file_headers[i].enc_len, NULL, NULL, 0);
		fseekass(fin, oar->file_headers[i].encloc, SEEK_SET);
		read_hash_write(fin, NULL, NULL, &sha256, tmp, oar->file_headers[i].enc_len);
		++i;
	}
	oar_sha256_final(scratch, &sha256);
	if(get_enc_size(oar)>0)
		assert(memcmp(scratch, oar->sha256+(2*32), 32)==0);

	/* Read padding */
	fseekass(fin, oar->padloc, SEEK_SET);
	freadass(scratch, 1, oar->usbloc[0]-oar->padloc, fin);
	i=0;
	while(i<oar->usbloc[0]-oar->padloc)
	{
		if(scratch[i]!=0)
			return oar_abort("Error: Post-ENC padding zeroes check failed, abort\n");
		++i;
	}

	/* bucket hash checks */
	i=0;
	while(i<4)
	{
		if(oar->bucket_sector_count[i])
		{
			oar_sha256_init(&sha256);
			fseekass(fin, oar->usbloc[i], SEEK_SET);
			j=0;
			while(j<oar->bucket_sector_count[i])
			{
				read_hash_write(fin, NULL, NULL, &sha256, scratch, bucketsize[i]);
				++j;
			}
			oar_sha256_final(scratch, &sha256);

			if(memcmp(scratch, oar->sha256+((i+3)*32), 32)==0)
			{
				if(ops->verbose)
					printf("bucket%"PRIu64" integrity check passed\n", bucketsize[i]);
			}
			else
			{
				printf("bucket%"PRIu64" integrity check ", bucketsize[i]);
				return oar_abort("failed, abort\n");
			}
		}

		++i;
	}

	printf("Success\n");
	free(scratch);
	free(str_builder);
	return 1;
}

