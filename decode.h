#ifndef OAR_DECODE
#define OAR_DECODE

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>

#include "cryptowrap.h"
#include "headers.h"

#include "filetypeiso.h"
#include "filetypebin.h"
#include "filetypengc.h"
#include "filetypeps3.h"
#include "filetypewii.h"

/* Main decode function */
int decode(struct oar_header *oar, struct oar_options *ops, char *infile);

/* Check hashes against footer and kick the tyres of the header */
int integrity(struct oar_header *oar, struct oar_options *ops, char* infile);

#ifdef __cplusplus
}
#endif

#endif

