#include "encode.h"
#include "common.h"
#include "filetype.h"

/*
	Encode paths

	1) Bin files may be present (read twice write once, seek many)

	* Potentially multiple buckets
	* The location of the buckets is unknown
	* Even after dumping YBE only the location of the first bucket is known

	Once order is established:
		* Do a pass over all input:
			* Dump ENC directly into archive
			* Add sector hashes to sector table. Periodically sort and remove duplicates to manage memory usage
			* Store YBE type byte for each sector
		* Do a seek pass over input for each data sector type in order:
			* Dump unique sectors directly to the archive
			* Build DLT
		* Write header, file_headers and dlt while calculating hashes
		* Write footer
*/
int encode(struct oar_header *oar, struct oar_options *ops)
{
	FILE *fin=0, *fout=0, *supplemental=0;
	struct sector *sectortable=0, *tmp_sector=0;
	uint8_t zero[16]={0};
	char *str_builder;

	/*
		enctype stores useful data from the first pass for use in the second:
		* BIN files use 2 bits per sector to store which bucket a sector is in
		* ISO files have no need for enctype data
		* NGC files use 1 bit per sector to determine padding modelling
		* PS3 files use 16 bytes to store the disc decryption key
		* WII files use 1 bit per sector to determine padding and group modelling
	*/
	uint8_t **enctype=0;
	uint8_t *dlt=0;

	struct file_list *curr_file_list=0;
	oar_sha1_ctx sha1;
	oar_sha256_ctx sha256;
	uint32_t b=0, bucketindex=0, sector_count=0;
	uint64_t dltloc=0, fileheadersloc=0, i=0, j=0, sectortable_len=0, sectortable_loc=0, enc_loc=0;
	int64_t enc_end, filelen=0;
	uint32_t bucketsize[4]={2048, 2324, 2336, 2352};

	uint32_t global_position;
	uint64_t bsize;

	uint64_t padding_size, sectablelastalloc=0;

	struct stats *stats=0;

	struct filetype_operations *fops=0;

	stats=mallocount(sizeof(struct stats), NULL, NULL);
	stats->memcurr=1024;

	fops=mallocount(FILE_TYPE_OP_ARRAY_SIZE*sizeof(struct filetype_operations), &stats->memcurr, &stats->mempeak);
	populate_filetype_ops(fops);

	str_builder=mallocount(FILENAME_STRING_LEN, &stats->memcurr, &stats->mempeak);

	/* Get list of files to add to archive */
	oar->filecount=ops->encode_files_count;
	if(oar->filecount==0) return oar_abort("Error: No input files found, abort\n");

	printf("> Archive %"PRIu64" images into '%s.oar'\n", oar->filecount, ops->outfile);

	oar->file_headers = mallocount(oar->filecount*sizeof(struct file_header), &stats->memcurr, &stats->mempeak);

	/* Read files from file_list */
	curr_file_list=ops->encode_files;
	i=0;
	while(i<oar->filecount)
	{
		oar->file_headers[i].filename=curr_file_list->filename;

		fin=fopenass(curr_file_list->filename, "rb");
		fseekass(fin, 0, SEEK_END);
		filelen=ftellass(fin);

		/* Determine file type */
		if(fops[FILE_TYPE_ISO].is(&oar->file_headers[i], fin))
		{
			oar->file_headers[i].type=FILE_TYPE_ISO;
			oar->file_headers[i].sector_count=filelen/2048;
			/* Check for specialised handling. Implementation assumes FILE_TYPE_* with values >=3 are specialisations of ISO */
			j=3;
			while(j<FILE_TYPE_OP_ARRAY_SIZE)
			{
				if(fops[j].is(&oar->file_headers[i], fin))
				{
					oar->file_headers[i].type=j;
					break;
				}
				++j;
			}
		}
		else if(fops[FILE_TYPE_BIN].is(&oar->file_headers[i], fin))
		{
			oar->file_headers[i].type=FILE_TYPE_BIN;
			oar->file_headers[i].sector_count=filelen/2352;
		}
		else
			return oar_abort("Error: Somehow a non iso/bin file has made it's way into the encode queue, abort\n");

		fclose(fin);

		sector_count+=oar->file_headers[i].sector_count;

		curr_file_list=curr_file_list->next;
		++i;
	}

	/* Sort files into their normalised order */
	if(ops->verbose)
		printf("Sort files into normalised order\n");
	qsort(oar->file_headers, oar->filecount, sizeof(struct file_header), compare_file);

	/* Determine where structures are located in archive */
	fileheadersloc=OAR_HEADER_LEN;
	dltloc=fileheadersloc+(oar->filecount*FILE_HEADER_LEN);
	enc_loc=sector_count;
	enc_loc*=4;
	enc_loc+=dltloc;

	/* Open archive for writing at ENC location */
	if(strlen(ops->outfile)>FILENAME_STRING_LEN-5)
		return oar_abort("Error: outfile name too long, abort\n");
	else
		sprintf(str_builder, "%s.oar", ops->outfile);

	fout=fopenass(str_builder, "wb");
	fseekass(fout, enc_loc, SEEK_SET);

	enctype=mallocount(sizeof(uint8_t *)*oar->filecount, &stats->memcurr, &stats->mempeak);

	oar_sha256_init(&sha256);

	if(ops->verbose)
		printf("First pass: Calculate ENC, build sector table, calculate SHA1 of input\n");
	/*
		* Do a pass over all input:
			* Dump YBE directly into archive
			* Add sector hashes to sector table. Periodically sort and remove duplicates to manage memory usage
			* Store YBE type byte for each sector
			* Calculate image SHA1
	*/
	global_position=0;
	i=0;
	while(i<oar->filecount)
	{
		/* Init common to all paths */
		fin=fopenass(oar->file_headers[i].filename, "rb");
		oar_sha1_init(&sha1);

		/* Resize sectortable to fit new image if it's not big enough */
		if(sectortable_loc+oar->file_headers[i].sector_count>sectortable_len)
		{
			sectortable=reallocount(sectortable, sizeof(struct sector)*(sectortable_loc+oar->file_headers[i].sector_count), &stats->memcurr, &stats->mempeak, sectablelastalloc);
			sectablelastalloc=sizeof(struct sector)*(sectortable_loc+oar->file_headers[i].sector_count);

			sectortable_len=sectortable_loc+oar->file_headers[i].sector_count;
		}

		/* Special handling not encapsulated by process(). Avoid implementing special handling here where possible as it's messy */
		printf("Process '%s'\n", oar->file_headers[i].filename);
		switch(oar->file_headers[i].type)
		{
			case FILE_TYPE_PS3:
				/* open IRD file */
				strcpy(str_builder, oar->file_headers[i].filename);
				assert(strlen(str_builder)>4);
				memcpy(str_builder+strlen(str_builder)-3, "ird", 3);
				supplemental=fopen(str_builder, "rb");
				if(supplemental==NULL)
				{
					printf("Error: Could not open '%s' file for reading, an IRD file is needed to process PS3 images\n", str_builder);
					exit(1);
				}
				break;
		}

		enctype[i]=fops[oar->file_headers[i].type].process(&oar->file_headers[i], stats, ops, fin, fout, supplemental, &sectortable[sectortable_loc], global_position, &sha1, &sha256);

		mempeak_update(&stats->memcurr, &stats->mempeak, stats->memret);
		global_position+=oar->file_headers[i].sector_count;
		oar->oar_filesize+=oar->file_headers[i].enc_len;
		stats->running_bytes_saved_by_modelling-=oar->file_headers[i].enc_len;

		/* De-duplicate sectortable */
		qsort(sectortable, sectortable_len, sizeof(struct sector), compare_sector);
		/* count unique sectors and mark duplicate sectors for deletion */
		sectortable_loc=1;
		tmp_sector=sectortable;
		j=1;
		while(j<sectortable_len)
		{
			if(compare_sector_hash(tmp_sector, &sectortable[j])==0)
				memset(sectortable[j].hash, 0xFF, 12);/* Duplicate, mark for removal */
			else
			{
				/* new sector */
				++sectortable_loc;
				tmp_sector=&sectortable[j];
			}
			++j;
		}
		/* resort sector table to move duplicate sectors to the end of array */
		qsort(sectortable, sectortable_len, sizeof(struct sector), compare_sector);

		++i;
	}
	oar_sha256_final(oar->sha256+(2*32), &sha256);

	/* Resize sectortable to remove any residual duplicate sectors */
	sectortable=reallocount(sectortable, sizeof(struct sector)*sectortable_loc, &stats->memcurr, &stats->mempeak, sectablelastalloc);
	sectortable_len=sectortable_loc;

	/*
		Add padding after ENC to align USB2048 to a 2 KiB boundary. This padding should allow decode performance to improve as all USB2048 reads should be contained in a single HDD sector instead of straddling two sectors.
			* HDDs typically have 4 KiB sectors
			* Decode is typically seek and read-single-sector heavy
	*/
	enc_end=ftellass(fout);
	padding_size=enc_end%2048;
	padding_size=2048-padding_size;
	if(padding_size==2048)
		padding_size=0;
	i=0;
	while(i<padding_size)
	{
		fwriteass(zero, 1, 1, fout);
		++i;
	}
	oar->oar_filesize+=padding_size;

	if(ops->verbose)
		printf("Second pass: Build DLT, copy unique sectors to archive\n");
	/*
		* Do a seek pass over input for each data sector type in order:
			* Build DLT on the fly
			* Dump unique sectors directly to the archive
	*/
	dlt=mallocount(4*sector_count, &stats->memcurr, &stats->mempeak);

	b=0;
	while(b<4)/* For every bucket in order */
	{
		printf("Gather USB%d\n", bucketsize[b]);
		global_position=0;
		bucketindex=0;
		oar_sha256_init(&sha256);/* Hash bucket */
		i=0;
		while(i<oar->filecount)/* For every image in order, find sectors that go into current bucket and gather them */
		{
			fin=fopenass(oar->file_headers[i].filename, "rb");

			/* Gather USB[bucketsize[b]] sectors for image i */
			fops[oar->file_headers[i].type].gather(&oar->file_headers[i], stats, ops, fin, sectortable, sectortable_len, global_position, &sha256, dlt, &bucketindex, fout, bucketsize[b], enctype[i]);

			/* Update global position so that DLT can get updated properly */
			global_position+=oar->file_headers[i].sector_count;

			fclose(fin);
			++i;
		}
		if(bucketindex)/* If anything has been written to this bucket */
			oar_sha256_final(oar->sha256+((b+3)*32), &sha256);

		/* Update header with bucket information */
		oar->bucket_sector_count[b]=bucketindex;
		bsize=bucketindex;
		bsize*=bucketsize[b];
		oar->oar_filesize+=bsize;

		++b;
	}

	if(ops->verbose)
		printf("Final phase: Write headers, lookup table and footer to archive\n");
	/* Fill out header */
	oar->oar_filesize+=OAR_HEADER_LEN;/* oar header */
	oar->oar_filesize+=(FILE_HEADER_LEN*oar->filecount);/* file header array */
	/* ybe done */
	oar->oar_filesize+=4*sector_count;/* data location array */
	/* buckets done */

	oar_sha256_init(&sha256);

	fseekass(fout, 0, SEEK_SET);

	/* header */
	if(!write_oar_header(oar, fout, &sha256)) return oar_abort("Error: write_oar_header() failed, abort\n");

	/* file header array */
	i=0;
	while(i<oar->filecount)
	{
		if(!write_file_header(&oar->file_headers[i], fout, &sha256)) return oar_abort("Error: write_file_header() failed, abort\n");
		++i;
	}
	oar_sha256_final(oar->sha256, &sha256);

	/* Write+hash DLT */
	fseekass(fout, dltloc, SEEK_SET);
	oar_sha256_init(&sha256);
	read_hash_write(NULL, fout, NULL, &sha256, dlt, 4*sector_count);
	oar_sha256_final(oar->sha256+(1*32), &sha256);

	if(ops->verbose)
		printf("Append footer\n");
	fseekass(fout, oar->oar_filesize, SEEK_SET);
	fwriteass(oar->sha256, 1, OAR_FOOTER_LEN, fout);
	fclose(fout);

	if(ops->verbose)
		printf("Create title file\n");

	assert(strlen(ops->outfile)+5<FILENAME_STRING_LEN);
	sprintf(str_builder, "%s.tit", ops->outfile);
	fout=fopenass(str_builder, "wb");
	write_titfile(fout, oar->file_headers, oar->filecount);
	fclose(fout);

	if(stats->running_bytes_saved_by_modelling!=0)
		printf("Amount saved by modelling: ~%"PRIi64" MiB\n", stats->running_bytes_saved_by_modelling/1048576);
	if(stats->running_bytes_decrypted_by_modelling!=0)
		printf("Amount decrypted by modelling: ~%"PRIi64" MiB\n", stats->running_bytes_decrypted_by_modelling/1048576);
	if(stats->running_bytes_saved_by_dedupe!=0)
		printf("Amount saved by deduplication: ~%"PRIi64" MiB\n", stats->running_bytes_saved_by_dedupe/1048576);
	printf("Peak memory usage: ~%"PRIu64" MiB\n", ((stats->mempeak/1048576)+1));
	printf("Success\n");

	free(fops);
	free(stats);
	free(str_builder);

	return 1;
}

