#ifndef OAR_ENCODE
#define OAR_ENCODE

#ifdef __cplusplus
extern "C" {
#endif

#include <inttypes.h>

#include "cryptowrap.h"
#include "headers.h"

#include "common.h"
#include "filetypeiso.h"
#include "filetypebin.h"
#include "filetypengc.h"
#include "filetypeps3.h"
#include "filetypewii.h"

/* main encode function */
int encode(struct oar_header *oar, struct oar_options *ops);

#ifdef __cplusplus
}
#endif

#endif
