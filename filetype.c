#include "filetype.h"
#include "filetypebin.h"
#include "filetypeiso.h"
#include "filetypengc.h"
#include "filetypeps3.h"
#include "filetypewii.h"


void populate_filetype_ops(struct filetype_operations *fops)
{
	fops[FILE_TYPE_BIN].is=is_bin;
	fops[FILE_TYPE_BIN].decode=decode_bin;
	fops[FILE_TYPE_BIN].gather=gather_bin;
	fops[FILE_TYPE_BIN].process=process_bin;
	fops[FILE_TYPE_BIN].read=read_bin;

	fops[FILE_TYPE_ISO].is=is_iso;
	fops[FILE_TYPE_ISO].decode=decode_iso;
	fops[FILE_TYPE_ISO].gather=gather_iso;
	fops[FILE_TYPE_ISO].process=process_iso;
	fops[FILE_TYPE_ISO].read=read_iso;

	fops[FILE_TYPE_NGC].is=is_ngc;
	fops[FILE_TYPE_NGC].decode=decode_ngc;
	fops[FILE_TYPE_NGC].gather=gather_ngc;
	fops[FILE_TYPE_NGC].process=process_ngc;
	fops[FILE_TYPE_NGC].read=read_ngc;

	fops[FILE_TYPE_PS3].is=is_ps3;
	fops[FILE_TYPE_PS3].decode=decode_ps3;
	fops[FILE_TYPE_PS3].gather=gather_ps3;
	fops[FILE_TYPE_PS3].process=process_ps3;
	fops[FILE_TYPE_PS3].read=read_ps3;

	fops[FILE_TYPE_WII].is=is_wii;
	fops[FILE_TYPE_WII].decode=decode_wii;
	fops[FILE_TYPE_WII].gather=gather_wii;
	fops[FILE_TYPE_WII].process=process_wii;
	fops[FILE_TYPE_WII].read=read_wii;
}
