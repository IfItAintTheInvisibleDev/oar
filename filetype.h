/**
 * @file Define filetype interface
 */
#ifndef OAR_FILETYPE
#define OAR_FILETYPE

#include <sys/types.h>
#include "common.h"

#define FILE_TYPE_ISO 1
#define FILE_TYPE_BIN 2
#define FILE_TYPE_NGC 3
#define FILE_TYPE_WII 4
#define FILE_TYPE_PS3 5
#define FILE_TYPE_OP_ARRAY_SIZE (FILE_TYPE_PS3+1)

#ifdef __cplusplus
extern "C" {
#endif

#define NGC_SECTOR_COUNT 712880
#define NGC_JUNK_BLOCK_SIZE 0x40000
/**
 * Structure holding working data to calculate padding on a NGC disc
 */
struct nod
{
	uint32_t buffer[0x824];/**< Internal buffer used by padding algorithm */
	uint8_t garbage_block[NGC_JUNK_BLOCK_SIZE];/**< The padding of the current block, this is what is present on disc if data isn't */
	uint8_t id[4];/**< The first 4 bytes of the image, used in the padding algorithm */
	uint8_t disc_number;/**< The disc number from the first sector of the image, used in the padding algorithm */
	uint8_t enc[NGC_SECTOR_COUNT];/**< The encoding data for a NGC image */
	uint32_t curr_blockcount;/**< The id corresponding to the current garbage block */
};

/* struct holding the encryption map to know which sectors are encrypted */
struct ps3
{
	uint32_t unencrypted_region_count;
	uint32_t encryption_map[511];
};

/* struct defining relevant information about an encoded Wii partition */
struct wiipartition
{
	uint8_t titlekey[16];
	uint32_t offset;
	uint32_t fullgroup_count;
	uint8_t *grouparray;
};

#define WII_CLUSTER_SIZE 32768
#define WII_SUBGROUP_SIZE (WII_CLUSTER_SIZE*8)
#define WII_GROUP_SIZE (WII_SUBGROUP_SIZE*8)
/* struct encapsulating memory needed to encode/decode a Wii image */
struct wii
{
	uint8_t commonkey[16];
	uint8_t koreacommonkey[16];
	uint8_t titlekey[16];

	uint32_t partid, groupid;
	uint8_t encrypted_group[WII_GROUP_SIZE];
	uint8_t decrypted_group[WII_GROUP_SIZE];

	uint32_t part_count;
	struct wiipartition *part_table;
	uint64_t padding_array_loc;
};

/* Holds working variables for handling a RAW sector */
struct ybe
{
	/* The sync data for comparison */
	uint8_t sync[12];
	/* Where an encode iteration puts its ybe data, and a decode iteration looks for ybe data from */
	uint8_t enc[292];
	/* Scratch space to convert integer address to BCD format */
	uint8_t add_scratch[3];
	/* The current sector */
	uint8_t sector[2352];
	/* Scratch space to scramble/unscramble without altering original data */
	uint8_t sector_scratch[2352];
	/* The data portion associated with the current sector */
	uint8_t data[2352];
	/* A full sector of zeroes for comparison */
	uint8_t zeroes[2352];

	/*
		Start at 150 and increment for every subsequent sector on a track. This models a normal track 1, encoding
		95%+ of all input efficiently. The rest only uses 3 bytes per sector and it's not worth complicating
		things to model more accurately.
	 */
	uint32_t sector_address;

	/* convenience variables to read data and encoding length without knowing ybe internals */
	uint32_t data_len;
	uint32_t enc_len;
};

struct filetype_operations
{
	/* Test that an image is this filetype */
	int (*is) (struct file_header *file_header, FILE *fin);
	/* Decode entire image into fout */
	void (*decode) (struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *bucketloc, uint8_t *enc);
	/* First encode pass over image, building ENC and acquiring image and sector metadata. Returns encoding details for use in gather function */
	uint8_t* (*process) (struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256);
	/* Pass over an input image, gathering sectors of a particular size to build DLT and USB* */
	void (*gather) (struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype);
	/* Decode an arbitrary chunk of an image */
	int (*read) (struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe);

};

void populate_filetype_ops(struct filetype_operations *fops);

#ifdef __cplusplus
}
#endif

#endif
