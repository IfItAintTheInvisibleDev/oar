/**
 * @file Implement functions for handling BIN images
 */

#include "filetypebin.h"
#include "common.h"

int read_bin(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe)
{
	int secsize;
	uint64_t j=0, bufferloc, windowstart, windowend;
	uint64_t start, end, trimstart, trimend, enc_loc=0;
	uint64_t data_len, curr_enc_len;

	assert(file_header!=NULL);
	assert(dlt!=NULL);
	assert(fin!=NULL);
	assert(usbloc!=NULL);
	assert(buffer!=NULL);

	secsize=file_header->type==FILE_TYPE_BIN?2352:2048;
	readmap(size, offset, secsize, &start, &end, &trimstart, &trimend);

	/* Check for over reading and truncate that puppy */
	if(end>file_header->sector_count)
	{
		end=file_header->sector_count;
		trimend=0;
	}

	bufferloc=0;

	j=0;
	while(j<end)
	{
		/* Read pertinent info from type byte in YBE encoding */
		if((data_len=ybe_type_to_data_len(enc[enc_loc]))==0)
			oar_abort("Error: ybe_type_to_data_len() returned 0, YBE error\n");
		curr_enc_len=ybe_type_to_enc_len(enc[enc_loc]);

		if(j>=start)/* If we're interested in this sector */
		{
			ybe->sector_address=j+150;

			/* Copy encoding to decoder */
			memcpy(ybe->enc, enc+enc_loc, curr_enc_len);

			/* Copy data to decoder */
			dataraw(j, dlt, fin, data_len, usbloc[sectorsize_to_index(data_len)], ybe->data);

			/* Decode */
			if(!decode_sector(ybe))
				oar_abort("Error: YBE decode error\n");

			/* Write to output */
			windowstart= (j==start?trimstart:0);
			windowend= (j==(end-1)?secsize-trimend:secsize);

			memcpy(buffer+bufferloc, ybe->sector+windowstart, windowend-windowstart);
			bufferloc+=(windowend-windowstart);
		}

		/* Update location in enc to the next sector */
		enc_loc+=curr_enc_len;
		++j;
	}

	return bufferloc;
}

void decode_bin(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *usbloc, uint8_t *enc)
{
	uint32_t curr_enc_len, data_len;
	uint64_t enc_loc=0, j=0;
	struct ybe *ybe=0;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sha1!=NULL);
	assert(dlt!=NULL);
	assert(usbloc!=NULL);
	assert(enc!=NULL);

	ybe=mallocount(sizeof(struct ybe), NULL, NULL);
	ybe_init(ybe);

	stats->memret=sizeof(struct ybe)+48;

	while(j<file_header->sector_count)
	{
		ybe->sector_address=j+150;
		/* Read pertinent info from type byte in YBE encoding */
		if((data_len=ybe_type_to_data_len(enc[enc_loc]))==0)
			oar_abort("Error: ybe_type_to_data_len() returned 0, YBE error\n");
		curr_enc_len=ybe_type_to_enc_len(enc[enc_loc]);
		/* Copy encoding to decoder */
		memcpy(ybe->enc, enc+enc_loc, curr_enc_len);

		/* Copy data to decoder */
		dataraw(j, dlt, fin, data_len, usbloc[sectorsize_to_index(data_len)], ybe->data);

		/* Decode */
		if(!decode_sector(ybe))
			oar_abort("Error: YBE decode error\n");
		/* Write to output */
		read_hash_write(NULL, fout, sha1, NULL, ybe->sector, 2352);
		/* Update location in enc to the next sector */
		enc_loc+=curr_enc_len;
		++j;
	}

	free(ybe);
}

void gather_bin(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype)
{
	int bucketdataindex;
	uint64_t j=0, seek=0;
	uint8_t digest[20];
	struct ybe *ybe=0;
	struct sector keysector;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sectortable!=NULL);
	assert(sha256!=NULL);
	assert(dlt!=NULL);
	assert(bucketindex!=NULL);
	assert(bucket!=NULL);
	assert(enctype!=NULL);
	assert(stats!=NULL);

	ybe=mallocount(sizeof(struct ybe), NULL, NULL);
	ybe_init(ybe);

	bucketdataindex=sectorsize_to_index(bucketsize);

	stats->memret=sizeof(struct ybe)+sizeof(struct sector)+56;

	while(j<file_header->sector_count)
	{
		if(bucketdataindex==(( enctype[j/4]>>(2*(j%4)) ) %4) )
		{
			/* this sector applies to the current bucket */

			/* Read the sector */
			seek=j;
			seek*=2352;
			fseekass(fin, seek, SEEK_SET);
			freadass(ybe->sector, 1, 2352, fin);

			/* Do encode and create digest of data */
			ybe->sector_address=j+150;
			if(!encode_sector(ybe))
				oar_abort("Error: YBE encode error\n");
			oar_sha1(ybe->data, ybe->data_len, digest);
			/* Search for sector in sectortable to build dlt entry */
			memcpy(keysector.hash, digest, 12);
			keysector.id=global_position+j;
			/* updatedlt */
			if(updatedlt(dlt, &keysector, sectortable, sectortable_len, bucketindex[0]))
			{
				/* unique sector, write it to bucket etc */
				bucketindex[0]++;
				read_hash_write(NULL, bucket, NULL, sha256, ybe->data, ybe->data_len);
			}
			else
			{
				stats->running_bytes_saved_by_dedupe+=ybe->data_len;
			}
		}
		++j;
	}
	free(ybe);

	/* Update peak memory stat */
	mempeak_update(&stats->memcurr, &stats->mempeak, stats->memret);

	if(bucketsize==2352)
	{
		/* As no more gather ops need to be done, free enctype and reflect that in memory stats */
		free(enctype);
		stats->memcurr-=((file_header->sector_count/4)+1);
	}
}

int is_bin(struct file_header *fh, FILE *fin)
{
	long size;

	assert(fh!=NULL);
	assert(fin!=NULL);

	if(strlen(fh->filename)>4 && strcmp(fh->filename+strlen(fh->filename)-4, ".bin")==0)
	{
		fseekass(fin, 0, SEEK_END);
		size=ftell(fin);
		rewind(fin);
		if(size%2352==0)
			return 1;
	}
	return 0;
}

uint8_t* process_bin(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256)
{
	uint32_t j=0;
	uint8_t digest[20];
	struct ybe *ybe=0;
	uint64_t unpredictablecounters[8]={0};
	uint8_t *enctype=0;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sectortable!=NULL);
	assert(foutencode!=NULL);
	assert(sha1!=NULL);
	assert(sha256!=NULL);
	assert(stats!=NULL);

	enctype=mallocount((file_header->sector_count/4)+1, &stats->memcurr, &stats->mempeak);

	ybe=mallocount(sizeof(struct ybe), NULL, NULL);
	ybe_init(ybe);

	stats->memret=sizeof(struct ybe)+100;

	file_header->enc_len=0;
	while(j<file_header->sector_count)
	{
		/* Read image, hash it if sha1 instance is present */
		read_hash_write(fin, NULL, sha1, NULL, ybe->sector, 2352);
		/* Do encode */
		ybe->sector_address=j+150;
		if(!encode_sector(ybe))
		{
			printf("Error: YBE encode error\n");
			exit(1);
		}

		if(ops->verbose)/* Collect stats if being verbose */
		{
			ybe_type_stat_collector(ybe->enc[0], unpredictablecounters);
			if(ybe_type_to_data_len(ybe->enc[0])==2048)
			{
				unpredictablecounters[0]+=is_eccp_zero(ybe);
				unpredictablecounters[1]+=is_eccq_zero(ybe);
			}
		}

		/* Process encode data (write enc to foutencode, hash with sha256) */
		read_hash_write(NULL, foutencode, NULL, sha256, ybe->enc, ybe->enc_len);
		file_header->enc_len+=ybe->enc_len;
		stats->running_bytes_saved_by_modelling+=(2352-ybe->data_len);

		enctype[j/4]+=((sectorsize_to_index(ybe_type_to_data_len(ybe->enc[0])))<<(2*(j%4)));

		oar_sha1(ybe->data, ybe->data_len, digest);
		memcpy(sectortable[j].hash, digest, 12);
		sectortable[j].id=global_position+j;

		++j;
	}
	fclose(fin);
	/* Finalise sha1 */
	oar_sha1_final(file_header->sha1, sha1);

	if(ops->verbose)/* Print prediction stats */
	{
		printf("Unpredictable stats\n");
		printf("Address: %"PRIu64"\n", unpredictablecounters[YBE_FLAG_ADDRESS]);
		printf("Subheader/Intermediate: %"PRIu64"\n", unpredictablecounters[YBE_FLAG_SUBHEADER]);
		printf("ECCP: %"PRIu64"\n", unpredictablecounters[YBE_FLAG_ECC_P]);
		printf("ECCQ: %"PRIu64"\n", unpredictablecounters[YBE_FLAG_ECC_Q]);
		printf("ECCP Zeroed: %"PRIu64"\n", unpredictablecounters[0]);
		printf("ECCQ Zeroed: %"PRIu64"\n", unpredictablecounters[1]);
		printf("EDC: %"PRIu64"\n", unpredictablecounters[YBE_FLAG_EDC]);
	}
	return enctype;
}

