/**
 * @file Define functions for handling BIN images
 */

#ifndef OAR_FILETYPEBIN
#define OAR_FILETYPEBIN

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "cryptowrap.h"
#include "filetype.h"
#include "headers.h"
#include "ybe.h"

int read_bin(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe);

void decode_bin(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *bucketloc, uint8_t *enc);

/* Read BIN sector by sector, updating dlt and writing unique sectors for a particular bucket */
void gather_bin(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype);

int is_bin(struct file_header *fh, FILE *fin);

/*
	Read a BIN file sector by sector and process it depending on what initialised variables are passed.
	Can be used by pass 1 of oar encode to process an image, or to generate an encoded stream for a
	standalone utility
 */
uint8_t* process_bin(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256);

#ifdef __cplusplus
}
#endif

#endif
