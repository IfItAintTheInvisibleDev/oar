/**
 * @file Implement functions for handling ISO images
 */
#include <stdio.h>
#include <sys/types.h>

#include "filetypeiso.h"
#include "common.h"

#include "filetype.h"

int read_iso(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe)
{
	int secsize, bufferloc=0;
	uint64_t j=0;
	uint64_t start, end, trimstart, trimend;
	uint8_t *buf=0;

	assert(file_header!=NULL);
	assert(dlt!=NULL);
	assert(fin!=NULL);
	assert(usbloc!=NULL);
	assert(buffer!=NULL);

	secsize=file_header->type==FILE_TYPE_BIN?2352:2048;
	readmap(size, offset, secsize, &start, &end, &trimstart, &trimend);

	/* Check for over reading and truncate that puppy */
	if(end>file_header->sector_count)
	{
		end=file_header->sector_count;
		trimend=0;
	}

	buf=mallocount(2048, NULL, NULL);

	bufferloc=0;

	j=start;
	while(j<end)
	{
		dataraw(j, dlt, fin, 2048, usbloc[0], buf);
		update_buffer(buffer, &bufferloc, j, start, end, trimstart, trimend, secsize, buf);
		++j;
	}

	free(buf);
	return bufferloc;
}

void decode_iso(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *usbloc, uint8_t *enc)
{
	uint64_t j=0, outputindex=0;
	uint8_t *outputbuffer=0;
	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sha1!=NULL);
	assert(dlt!=NULL);
	assert(usbloc!=NULL);

	outputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);

	stats->memret=48+SECTORBUFFERSIZE;

	while(j<file_header->sector_count)
	{
		dataraw(j, dlt, fin, 2048, usbloc[0], outputbuffer+(outputindex*2048));

		/* hash and write to output */
		++outputindex;
		if(outputindex==(SECTORBUFFERSIZE/2048))/* Flush output buffer */
		{
			read_hash_write(NULL, fout, sha1, NULL, outputbuffer, SECTORBUFFERSIZE);
			outputindex=0;
		}
		++j;
	}
	if(outputindex)
		read_hash_write(NULL, fout, sha1, NULL, outputbuffer, outputindex*2048);

	free(outputbuffer);
}

void gather_iso(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype)
{
	uint64_t i=0, j=0, fullbuffers, partialbuffer, sectorindex=0, outputindex=0;
	uint8_t digest[20];
	struct sector keysector;
	uint8_t *inputbuffer;
	uint8_t *outputbuffer;

	/* Early exit for sector sizes that cannot be stored in an ISO image */
	if(bucketsize!=2048)
		return;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sectortable!=NULL);
	assert(sha256!=NULL);
	assert(dlt!=NULL);
	assert(bucketindex!=NULL);
	assert(bucket!=NULL);
	assert(stats!=NULL);

	inputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);
	outputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);

	stats->memret=100+(2*SECTORBUFFERSIZE);

	fullbuffers=file_header->sector_count/(SECTORBUFFERSIZE/2048);
	partialbuffer=file_header->sector_count%(SECTORBUFFERSIZE/2048);
	i=0;
	while(i<fullbuffers+1)
	{
		/* Refill input buffer */
		read_hash_write(fin, NULL, NULL, NULL, inputbuffer, (i==fullbuffers?partialbuffer*2048:(SECTORBUFFERSIZE/2048)*2048));
		/* Process sectors in input buffer */
		j=0;
		while( j<(i==fullbuffers?partialbuffer:(SECTORBUFFERSIZE/2048)) )
		{
			oar_sha1(inputbuffer+(j*2048), 2048, digest);
			/* Search for sector in sectortable to build dlt entry */
			memcpy(keysector.hash, digest, 12);
			keysector.id=global_position+sectorindex;
			/* updatedlt */
			if(updatedlt(dlt, &keysector, sectortable, sectortable_len, bucketindex[0]))
			{
				/* unique sector, write it to bucket etc */
				bucketindex[0]++;
				memcpy(outputbuffer+(outputindex*2048), inputbuffer+(j*2048), 2048);
				++outputindex;
				if(outputindex==(SECTORBUFFERSIZE/2048))/* Flush output buffer */
				{
					read_hash_write(NULL, bucket, NULL, sha256, outputbuffer, SECTORBUFFERSIZE);
					outputindex=0;
				}
			}
			else
			{
				stats->running_bytes_saved_by_dedupe+=2048;
			}
			++j;
			++sectorindex;
		}
		++i;
	}
	if(outputindex)
		read_hash_write(NULL, bucket, NULL, sha256, outputbuffer, outputindex*2048);

	free(inputbuffer);
	free(outputbuffer);

	/* Update peak memory stat */
	mempeak_update(&stats->memcurr, &stats->mempeak, stats->memret);
}

int is_iso(struct file_header *fh, FILE *fin)
{
	long size;

	assert(fh!=NULL);
	assert(fin!=NULL);

	if(strlen(fh->filename)>4 && strcmp(fh->filename+strlen(fh->filename)-4, ".iso")==0)
	{
		fseekass(fin, 0, SEEK_END);
		size=ftell(fin);
		rewind(fin);
		if(size%2048==0)
			return 1;
	}
	return 0;
}

uint8_t* process_iso(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256)
{
	uint64_t i=0, j=0, fullbuffers, partialbuffer, sectorindex=0;
	uint8_t digest[20];
	uint8_t *inputbuffer;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sectortable!=NULL);
	assert(sha1!=NULL);


	inputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);

	stats->memret=80+SECTORBUFFERSIZE;

	fullbuffers=file_header->sector_count/(SECTORBUFFERSIZE/2048);
	partialbuffer=file_header->sector_count%(SECTORBUFFERSIZE/2048);
	i=0;
	while(i<fullbuffers+1)
	{
		/* Refill input buffer */
		read_hash_write(fin, NULL, sha1, NULL, inputbuffer, (i==fullbuffers?partialbuffer*2048:(SECTORBUFFERSIZE/2048)*2048));
		/* Process sectors in input buffer */
		j=0;
		while( j<(i==fullbuffers?partialbuffer:(SECTORBUFFERSIZE/2048)) )
		{
			/* sector hash */
			oar_sha1(inputbuffer+(j*2048), 2048, digest);
			/* add to sector table */
			memcpy(sectortable[sectorindex].hash, digest, 12);
			sectortable[sectorindex].id=global_position+sectorindex;

			++j;
			++sectorindex;
		}
		++i;
	}
	/* Finalise anything that needs finalising */
	fclose(fin);
	oar_sha1_final(file_header->sha1, sha1);
	free(inputbuffer);

	return NULL;
}
