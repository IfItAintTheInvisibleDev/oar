/**
 * @file Define functions for handling ISO images
 */

#ifndef OAR_FILETYPEISO
#define OAR_FILETYPEISO

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cryptowrap.h"
#include "common.h"
#include "filetype.h"
#include "headers.h"

/*int read_iso(char *buffer, size_t size, off_t offset);*/

void decode_iso(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *bucketloc, uint8_t *enc);

/* Read ISO sector by sector, updating dlt and writing unique sectors for a particular bucket */
void gather_iso(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype);

int is_iso(struct file_header *fh, FILE *fin);

/*
	Read an ISO file sector by sector and process it depending on what initialised variables are passed.
	Can be used by pass 1 of oar encode to process an image, or to generate an encoded stream for a
	standalone utility
 */
uint8_t* process_iso(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256);

int read_iso(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe);

#ifdef __cplusplus
}
#endif

#endif

