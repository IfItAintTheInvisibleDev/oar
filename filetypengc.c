/**
 * @file Implement functions for handling NGC images
 */

#include "filetypengc.h"
#include "common.h"

/* Read sector at index into buffer */
void sector_ngc(uint64_t index, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t usbloc, uint8_t *buffer, struct nod *nod)
{
	if( (enc[index/8]>>(index%8))%2 )
	{
		/* Data was predicted, find it */
		/* Update junk block if necessary */
		if(nod->curr_blockcount!=(index/128))
			get_junk_block(nod, index/128);
		memcpy(buffer, nod->garbage_block+((index%128)*2048), 2048);
	}
	else
	{
		/* Copy data from bucket */
		dataraw(index, dlt, fin, 2048, usbloc, buffer);
	}
}

int read_ngc(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe)
{
	int secsize;
	uint64_t j=0, bufferloc, windowstart, windowend;
	uint64_t start, end, trimstart, trimend;
	uint8_t *buf=0;

	assert(file_header!=NULL);
	assert(dlt!=NULL);
	assert(fin!=NULL);
	assert(usbloc!=NULL);
	assert(buffer!=NULL);

	secsize=file_header->type==FILE_TYPE_BIN?2352:2048;
	readmap(size, offset, secsize, &start, &end, &trimstart, &trimend);
	/* Check for over reading and truncate that puppy */
	if(end>file_header->sector_count)
	{
		end=file_header->sector_count;
		trimend=0;
	}

	buf=mallocount(2048, NULL, NULL);
	bufferloc=0;

	j=start;
	while(j<end)
	{
		sector_ngc(j, dlt, enc, fin, usbloc[0], buf, nod);

		windowstart= (j==start?trimstart:0);
		windowend= (j==(end-1)?secsize-trimend:secsize);

		memcpy(buffer+bufferloc, buf+windowstart, windowend-windowstart);
		bufferloc+=(windowend-windowstart);

		++j;
	}

	free(buf);
	return bufferloc;
}

void decode_ngc(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *bucketloc, uint8_t *enc)
{
	uint8_t *sector;
	uint64_t j=0, seek=0;
	struct nod *nod=0;
	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sha1!=NULL);
	assert(dlt!=NULL);
	assert(bucketloc!=NULL);
	assert(enc!=NULL);
	assert(stats!=NULL);

	sector=mallocount(2048, NULL, NULL);
	nod=mallocount(sizeof(struct nod), NULL, NULL);

	stats->memret=2048+sizeof(struct nod)+40;

	while(j<file_header->sector_count)
	{
		if( (enc[j/8]>>(j%8))%2 )
		{
			/* Data was predicted, find it */
			/* Update junk block if necessary */
			if(nod->curr_blockcount!=(j/128))
				get_junk_block(nod, j/128);
			read_hash_write(NULL, fout, sha1, NULL, nod->garbage_block+((j%128)*2048), 2048);
		}
		else
		{
			/* Copy data from bucket */
			seek=2048;
			seek*=get32lsb(dlt+4*j);
			seek+=bucketloc[0];
			fseekass(fin, seek, SEEK_SET);
			/* Read from bucket, hash, write to output */
			read_hash_write(fin, fout, sha1, NULL, sector, 2048);
			if(j==0)
			{
				/* First sector, intialise padding modelling */
				memcpy(nod->id, sector, 4);
				nod->disc_number=sector[6];
				get_junk_block(nod, 0);
			}
		}
		++j;
	}
	free(sector);
}

void gather_ngc(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype)
{
	uint64_t j=0, seek=0;
	uint8_t *sector;
	uint8_t digest[20];
	uint8_t *zero;
	uint8_t zerohash[20];
	struct sector keysector;

	/* Early exit for sector sizes that cannot be stored in an ISO image */
	if(bucketsize!=2048)
		return;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sectortable!=NULL);
	assert(sha256!=NULL);
	assert(dlt!=NULL);
	assert(bucketindex!=NULL);
	assert(bucket!=NULL);
	assert(enctype!=NULL);
	assert(stats!=NULL);

	sector=mallocount(2048, NULL, NULL);
	zero=mallocount(2048, NULL, NULL);

	stats->memret=92+4096+16;

	oar_sha1(zero, 2048, zerohash);

	while(j<file_header->sector_count)
	{
		if((enctype[j/8]>>(j%8))%2)
		{
			/* Predictable */
			memset(sector, 0, 2048);
			memcpy(digest, zerohash, 20);
		}
		else
		{
			/* Unpredictable, treat as ISO sector */
			seek=j;
			seek*=2048;
			fseekass(fin, seek, SEEK_SET);
			freadass(sector, 1, 2048, fin);
			oar_sha1(sector, 2048, digest);
		}

		/* Search for sector in sectortable to build dlt entry */
		memcpy(keysector.hash, digest, 12);
		keysector.id=global_position+j;
		/* updatedlt */
		if(updatedlt(dlt, &keysector, sectortable, sectortable_len, bucketindex[0]))
		{
			/* unique sector, write it to bucket etc */
			bucketindex[0]++;
			read_hash_write(NULL, bucket, NULL, sha256, sector, 2048);
		}
		else
		{
			if((enctype[j/8]>>(j%8))%2==0)
				stats->running_bytes_saved_by_dedupe+=2048;
		}
		++j;
	}
	free(sector);
	free(zero);

	/* Update peak memory stat */
	mempeak_update(&stats->memcurr, &stats->mempeak, stats->memret);

	/* As no more gather ops need to be done, free enctype and reflect that in memory stats */
	free(enctype);
	stats->memcurr-=((file_header->sector_count/8)+1);
}

int is_ngc(struct file_header *file_header, FILE *fin)
{
	int64_t seek=0;
	uint8_t *sector;
	assert(fin!=NULL);

	sector=mallocount(2048, NULL, NULL);

	fseekass(fin, 0, SEEK_END);
	seek=ftellass(fin);
	if(seek!=NGC_SECTOR_COUNT*2048)
	{
		free(sector);
		return 0;
	}

	fseekass(fin, 0, SEEK_SET);
	freadass(sector, 1, 2048, fin);
	if(nod_read_header(sector)==FILE_TYPE_NGC)
	{
		free(sector);
		return 1;
	}

	free(sector);
	return 0;
}

uint8_t* process_ngc(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256)
{
	uint32_t j=0;
	uint8_t digest[20];
	uint8_t *sector;
	uint8_t *zero;
	uint8_t zerohash[20];
	struct nod *nod;
	uint8_t *enctype=0;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(foutencode!=NULL);
	assert(sectortable!=NULL);
	assert(enctype!=NULL);
	assert(sha1!=NULL);
	assert(sha256!=NULL);
	assert(stats!=NULL);

	enctype=mallocount((file_header->sector_count/8)+1, &stats->memcurr, &stats->mempeak);

	sector=mallocount(2048, NULL, NULL);
	zero=mallocount(2048, NULL, NULL);
	nod=mallocount(sizeof(struct nod), NULL, NULL);

	stats->memret=80+sizeof(struct nod)+4096;

	oar_sha1(zero, 2048, zerohash);

	while(j<file_header->sector_count)
	{
		/* Read + image hash */
		read_hash_write(fin, NULL, sha1, NULL, sector, 2048);

		/* Determine if sector is predictable */
		if(j==0)
		{
			/* First sector, seed padding algorithm with required data */
			memcpy(nod->id, sector, 4);
			nod->disc_number=sector[6];
		}
		if(j%128==0)
			get_junk_block(nod, j/128);/* Calculate next junk block if necessary */
		if(memcmp(sector, nod->garbage_block+(2048*(j%128)), 2048)==0)
		{
			/* Predicted, add zeroed sector to sectortable and set encoding accordingly */
			enctype[j/8]+=1<<(j%8);
			memcpy(digest, zerohash, 20);
			stats->running_bytes_saved_by_modelling+=2048;
		}
		else
		{
			/* Unpredictable, add sector to sectortable */
			/* sector hash */
			oar_sha1(sector, 2048, digest);
		}

		/* add to sector table */
		memcpy(sectortable[j].hash, digest, 12);
		sectortable[j].id=global_position+j;

		++j;
	}
	/* Finalise anything that needs finalising */
	fclose(fin);
	oar_sha1_final(file_header->sha1, sha1);
	/* Write encoding to file */
	read_hash_write(NULL, foutencode, NULL, sha256, enctype, NGC_SECTOR_COUNT/8);
	file_header->enc_len=NGC_SECTOR_COUNT/8;
	free(sector);
	free(zero);
	return enctype;
}

