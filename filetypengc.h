/**
 * @file Define functions for handling NGC images
 */
#ifndef OAR_FILETYPENGC
#define OAR_FILETYPENGC

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cryptowrap.h"
#include "common.h"
#include "filetype.h"
#include "headers.h"
#include "nod.h"

/** Takes the global DLT, global USB and local ENC encoding and decodes back to the original NGC image */
void decode_ngc(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *bucketloc, uint8_t *enc);

/** Takes an input stream and determines if it's an NGC image */
int is_ngc(struct file_header *file_header, FILE *fin);

/* Read NGC sector by sector, updating dlt and writing unique sectors for a particular bucket */
void gather_ngc(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype);

/*
	Read a NGC file sector by sector and process it depending on what initialised variables are passed.
	Can be used by pass 1 of oar encode to process an image, or to generate an encoded stream for a
	standalone utility
 */
uint8_t* process_ngc(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256);

int read_ngc(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe);

#ifdef __cplusplus
}
#endif

#endif
