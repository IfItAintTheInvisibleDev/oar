/**
 * @file Implement functions for handling PS3 images
 */

#include <zlib.h>
#include "filetypeps3.h"
#include "common.h"

#define STATE_ENCRYPTED 0
#define STATE_NOTENCRYPTED 1

int getiv_ps3(uint32_t index, uint8_t *iv)
{
	uint32_t use;
	int i=0;
	assert(iv!=NULL);
	use=index;
	while(i<16)
	{
		iv[15-i] = use%256;
		use/=256;
		++i;
	}
	return 1;
}

int is_encrypted(uint64_t index, struct ps3 *ps3)
{
	uint8_t state=STATE_ENCRYPTED;
	int i=0;
	while(i<510)
	{
		if(index>=ps3->encryption_map[i])
		{
			state=(state+1)%2;
			++i;
		}
		else
			return state==STATE_ENCRYPTED;
	}
	return state==STATE_ENCRYPTED;/* Should never hit */
}

int decrypt_sector_ps3(uint8_t *enc, uint8_t *dec, uint8_t *disckey, uint32_t index)
{
	uint8_t iv[16];
	getiv_ps3(index, iv);
	oar_aes128cbc_decrypt(disckey, iv, enc, dec, 2048);
	return 1;
}

int encrypt_sector_ps3(uint8_t *enc, uint8_t *dec, uint8_t *disckey, uint32_t index)
{
	uint8_t iv[16];
	getiv_ps3(index, iv);
	oar_aes128cbc_encrypt(disckey, iv, enc, dec, 2048);
	return 1;
}

/* Parse the first sector of the image into an encryption map */
void populate_encryption_map(uint8_t *sector, struct ps3 *ps3)
{
	int i;
	assert(sector!=NULL);
	assert(ps3!=NULL);
	ps3->unencrypted_region_count=get32msb(sector);
	i=0;
	while(i<510)
	{
		ps3->encryption_map[i]=get32msb(sector+8+(i*4));
		++i;
	}
	ps3->encryption_map[i]=0;
}

int read_ps3(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe)
{
	int secsize;
	uint64_t j=0, bufferloc, windowstart, windowend;
	uint64_t start, end, trimstart, trimend;
	uint8_t *buf=0;

	assert(file_header!=NULL);
	assert(dlt!=NULL);
	assert(fin!=NULL);
	assert(usbloc!=NULL);
	assert(buffer!=NULL);
	assert(ps3!=NULL);

	secsize=file_header->type==FILE_TYPE_BIN?2352:2048;
	readmap(size, offset, secsize, &start, &end, &trimstart, &trimend);

	/* Check for over reading and truncate that puppy */
	if(end>file_header->sector_count)
	{
		end=file_header->sector_count;
		trimend=0;
	}

	buf=mallocount(4096, NULL, NULL);
	bufferloc=0;

	j=start;
	while(j<end)
	{
		dataraw(j, dlt, fin, 2048, usbloc[0], buf);

		if(is_encrypted(j, ps3))
		{
			memcpy(buf+2048, buf, 2048);
			encrypt_sector_ps3(buf, buf+2048, enc, j);
		}

		windowstart= (j==start?trimstart:0);
		windowend= (j==(end-1)?secsize-trimend:secsize);

		memcpy(buffer+bufferloc, buf+windowstart, windowend-windowstart);
		bufferloc+=(windowend-windowstart);

		++j;
	}

	free(buf);
	return bufferloc;
}

/** Parse IRD from char array and extract data1, from which the disc decryption key is derived */
int ird_parse(uint8_t *ird, uint64_t irdlen, uint8_t *key, uint8_t *ps3key, uint8_t *ps3iv)
{
	uint8_t *use, *decomp=0;
	int64_t uselen=0, data1loc;
	z_stream infstream;
	assert(ird!=NULL);
	assert(key!=NULL);
	assert(ps3key!=NULL);
	assert(ps3iv!=NULL);

	/* Decompress if compressed */
	if(ird[0]==0x1F && ird[1]==0x8B)
	{
		decomp=mallocount(1048576*16, NULL, NULL);

		infstream.zalloc=Z_NULL;
		infstream.zfree=Z_NULL;
		infstream.opaque=Z_NULL;

		infstream.avail_in = irdlen;
		infstream.next_in = ird;

		infstream.avail_out = 1048576*16;
		infstream.next_out = decomp;

		inflateInit2(&infstream, 15+32);
		inflate(&infstream, Z_FINISH);
		inflateEnd(&infstream);
		uselen=infstream.total_out;

		assert(uselen>0);

		decomp=reallocount(decomp, uselen, NULL, NULL, 0);
		use=decomp;
	}
	else
	{
		use=ird;
		uselen=irdlen;
	}

	assert(memcmp(use, "3IRD", 4)==0);
	assert(use[4]>=6 && use[4]<=9);

	data1loc=uselen-40;
	if(use[4]<9)
		data1loc-=115;

	assert(data1loc>=0);

	oar_aes128cbc_encrypt(ps3key, ps3iv, key, use+data1loc, 16);

	if(decomp)
		free(decomp);
	return 1;
}

int is_ps3(struct file_header *file_header, FILE *fin)
{
	uint8_t tmp[12];
	assert(fin!=NULL);
	rewind(fin);
	fseekass(fin, 2048, SEEK_SET);
	freadass(tmp, 1, 12, fin);
	return (memcmp(tmp, "PlayStation3", 12)==0)?1:0;
}

void decode_ps3(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *usbloc, uint8_t *enc)
{
	uint64_t j=0, outputindex=0;
	uint8_t *outputbuffer, *sec;
	uint32_t encryption_map_loc=0;
	uint8_t state=STATE_ENCRYPTED;
	struct ps3 *ps3=0;

	assert(dlt!=NULL);
	assert(enc!=NULL);
	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sha1!=NULL);
	assert(usbloc!=NULL);

	outputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);
	ps3=mallocount(sizeof(struct ps3), NULL, NULL);
	sec=mallocount(2048, NULL, NULL);

	stats->memret=SECTORBUFFERSIZE+2048+128;

	while(j<file_header->sector_count)
	{
		dataraw(j, dlt, fin, 2048, usbloc[0], sec);

		if(j==0)/* First sector, read encryption map */
			populate_encryption_map(sec, ps3);
		/* Switch from encrypted block to decrypted block or vice-versa based on the encryption map */
		if(j==ps3->encryption_map[encryption_map_loc])
		{
			state=(state+1)%2;
			++encryption_map_loc;
		}
		if(state==STATE_ENCRYPTED)/* Sector originally encrypted */
			encrypt_sector_ps3(outputbuffer+(outputindex*2048), sec, enc, j);
		else/* Sector originally not encrypted */
			memcpy(outputbuffer+(outputindex*2048), sec, 2048);

		/* hash and write to output */
		++outputindex;
		if(outputindex==(SECTORBUFFERSIZE/2048))/* Flush output buffer */
		{
			read_hash_write(NULL, fout, sha1, NULL, outputbuffer, SECTORBUFFERSIZE);
			outputindex=0;
		}
		++j;
	}
	if(outputindex)
		read_hash_write(NULL, fout, sha1, NULL, outputbuffer, outputindex*2048);

	free(outputbuffer);
	free(ps3);
	free(sec);
}

void gather_ps3(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype)
{
	uint64_t i=0, j=0, fullbuffers, partialbuffer, sectorindex=0, outputindex=0;
	uint8_t digest[20];
	struct sector keysector;
	uint8_t *inputbuffer;
	uint8_t *outputbuffer;

	uint32_t encryption_map_loc=0;
	uint8_t state=STATE_ENCRYPTED;
	uint8_t *decsec;
	struct ps3 *ps3=0;

	/* Early exit for sector sizes that cannot be stored in an ISO image */
	if(bucketsize!=2048)
		return;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sectortable!=NULL);
	assert(sha256!=NULL);
	assert(dlt!=NULL);
	assert(bucketindex!=NULL);
	assert(bucket!=NULL);
	assert(enctype!=NULL);
	assert(stats!=NULL);

	decsec=mallocount(2048, NULL, NULL);
	inputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);
	outputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);
	ps3=mallocount(sizeof(struct ps3), NULL, NULL);

	fullbuffers=file_header->sector_count/(SECTORBUFFERSIZE/2048);
	partialbuffer=file_header->sector_count%(SECTORBUFFERSIZE/2048);
	i=0;
	while(i<fullbuffers+1)
	{
		/* Refill input buffer */
		read_hash_write(fin, NULL, NULL, NULL, inputbuffer, (i==fullbuffers?partialbuffer*2048:(SECTORBUFFERSIZE/2048)*2048));
		/* Process sectors in input buffer */
		j=0;
		while( j<(i==fullbuffers?partialbuffer:(SECTORBUFFERSIZE/2048)) )
		{
			if(sectorindex==0)
				populate_encryption_map(inputbuffer+(j*2048), ps3);

			/* Switch from encrypted block to decrypted block or vice-versa based on the encryption map */
			if(sectorindex==ps3->encryption_map[encryption_map_loc])
			{
				state=(state+1)%2;
				++encryption_map_loc;
			}
			/* If in an encrypted block, decrypt */
			if(state==STATE_ENCRYPTED)
			{
				decrypt_sector_ps3(inputbuffer+(j*2048), decsec, enctype, sectorindex);
				/* sector hash */
				oar_sha1(decsec, 2048, digest);
			}
			else
			{
				/* sector hash */
				oar_sha1(inputbuffer+(j*2048), 2048, digest);
			}

			/* Search for sector in sectortable to build dlt entry */
			memcpy(keysector.hash, digest, 12);
			keysector.id=global_position+sectorindex;
			/* updatedlt */
			if(updatedlt(dlt, &keysector, sectortable, sectortable_len, bucketindex[0]))
			{
				/* unique sector, write it to bucket etc */
				bucketindex[0]++;
				/* If encrypted, write decrypted form */
				memcpy(outputbuffer+(outputindex*2048), state==STATE_ENCRYPTED?decsec:inputbuffer+(j*2048), 2048);
				++outputindex;
				if(outputindex==(SECTORBUFFERSIZE/2048))/* Flush output buffer */
				{
					read_hash_write(NULL, bucket, NULL, sha256, outputbuffer, SECTORBUFFERSIZE);
					outputindex=0;
				}
			}
			else
			{
				stats->running_bytes_saved_by_dedupe+=2048;
			}
			++j;
			++sectorindex;
		}
		++i;
	}
	if(outputindex)
		read_hash_write(NULL, bucket, NULL, sha256, outputbuffer, outputindex*2048);

	free(decsec);
	free(inputbuffer);
	free(outputbuffer);
	free(ps3);

	/* Update peak memory stat */
	mempeak_update(&stats->memcurr, &stats->mempeak, stats->memret);

	/* As no more gather ops need to be done, free enctype and reflect that in memory stats */
	free(enctype);
	stats->memcurr-=16;
}

uint8_t* process_ps3(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256)
{
	uint64_t i=0, j=0, fullbuffers, partialbuffer, sectorindex=0, irdlen=0;
	uint8_t digest[20];
	uint8_t *inputbuffer, *ird=0;
	uint32_t encryption_map_loc=0;
	uint8_t state=STATE_ENCRYPTED;
	uint8_t *decsec;
	uint8_t zero[16]={0};
	uint8_t *enctype=0;
	struct ps3 *ps3=0;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(foutencode!=NULL);
	assert(sectortable!=NULL);
	assert(enctype!=NULL);
	assert(sha1!=NULL);
	assert(sha256!=NULL);
	assert(stats!=NULL);

	enctype=mallocount(16, &stats->memcurr, &stats->mempeak);
	ps3=mallocount(sizeof(struct ps3), NULL, NULL);

	if(memcmp(ops->ps3key, zero, 16)==0 || memcmp(ops->ps3iv, zero, 16)==0)
	{
		printf("Error: User attempting to encode PS3 images without supplying the common key and iv, abort\n");
		exit(1);
	}

	/* Read IRD from file */
	fseekass(supplemental, 0, SEEK_END);
	irdlen=ftellass(supplemental);
	rewind(supplemental);

	ird=mallocount(irdlen, NULL, NULL);
	freadass(ird, 1, irdlen, supplemental);
	fclose(supplemental);

	/* Parse IRD to get disc key, store in enctype */
	ird_parse(ird, irdlen, enctype, ops->ps3key, ops->ps3iv);

	/* Encoding is disc key followed by IRD used to derive it */
	read_hash_write(NULL, foutencode, NULL, sha256, enctype, 16);
	read_hash_write(NULL, foutencode, NULL, sha256, ird, irdlen);
	file_header->enc_len=16+irdlen;
	free(ird);

	decsec=mallocount(2048, NULL, NULL);
	inputbuffer=mallocount(SECTORBUFFERSIZE, NULL, NULL);

	fullbuffers=file_header->sector_count/(SECTORBUFFERSIZE/2048);
	partialbuffer=file_header->sector_count%(SECTORBUFFERSIZE/2048);
	i=0;
	while(i<fullbuffers+1)
	{
		/* Refill input buffer */
		read_hash_write(fin, NULL, sha1, NULL, inputbuffer, (i==fullbuffers?partialbuffer*2048:(SECTORBUFFERSIZE/2048)*2048));
		/* Process sectors in input buffer */
		j=0;
		while( j<(i==fullbuffers?partialbuffer:(SECTORBUFFERSIZE/2048)) )
		{
			if(sectorindex==0)
				populate_encryption_map(inputbuffer+(j*2048), ps3);

			/* Switch from encrypted block to decrypted block or vice-versa based on the encryption map */
			if(sectorindex==ps3->encryption_map[encryption_map_loc])
			{
				state=(state+1)%2;
				++encryption_map_loc;
			}
			/* If in an encrypted block, decrypt */
			if(state==STATE_ENCRYPTED)
			{
				decrypt_sector_ps3(inputbuffer+(j*2048), decsec, enctype, sectorindex);
				/* sector hash */
				oar_sha1(decsec, 2048, digest);
			}
			else
			{
				/* sector hash */
				oar_sha1(inputbuffer+(j*2048), 2048, digest);
			}

			/* add to sector table */
			memcpy(sectortable[sectorindex].hash, digest, 12);
			sectortable[sectorindex].id=global_position+sectorindex;

			++j;
			++sectorindex;
		}
		++i;
	}
	/* Finalise anything that needs finalising */
	fclose(fin);
	oar_sha1_final(file_header->sha1, sha1);
	free(decsec);
	free(inputbuffer);
	free(ps3);
	return enctype;
}

