/**
 * @file Define functions for handling PS3 images
 */

#ifndef OAR_FILETYPEPS3
#define OAR_FILETYPEPS3

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cryptowrap.h"
#include "common.h"
#include "filetype.h"
#include "headers.h"

/* Parse the first sector of an image into an encryption map */
void populate_encryption_map(uint8_t *sector, struct ps3 *ps3);

int read_ps3(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe);

void decode_ps3(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *bucketloc, uint8_t *enc);

/* Read PS3 sector by sector, updating dlt and writing unique sectors for a particular bucket */
void gather_ps3(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype);

/** Takes an input stream and determines if it's a PS3 image */
int is_ps3(struct file_header *file_header, FILE *fin);

/*
	Process a PS3 image:
		* SHA1 the image
		* Build sectortable
		* Write ENC to the archive
 */
uint8_t* process_ps3(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256);

#ifdef __cplusplus
}
#endif

#endif

