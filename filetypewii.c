/**
 * @file Implement functions for handling WII images
 */

#include "filetypewii.h"
#include "common.h"

/*
	Wii encoding format:
	* uint32_t num partitions
	* For each partition:
		* 16 byte title key
		* uint32_t Offset to first group
		* uint64_t Number of full groups in partition
		* Bit array for full groups in partition showing:
			* 0 for stored as-is, 1 for stored fully decoded (decrypted and sha1 chunk zeroed)
	* Bit array for all remaining sectors (sectors not covered by full groups)
		* 0 for stored as-is, 1 for padding can be predicted and stored zeroed
 */

/** Comparator for sorting partitions into order encountered when image read start to finish */
int wiipartition_compare(const void *a, const void *b)
{
	struct wiipartition *af=((struct wiipartition *)a);
	struct wiipartition *bf=((struct wiipartition *)b);

	if(af->offset==bf->offset)
		return 0;
	else
		return af->offset<bf->offset?-1:1;
}

/* Locations and sizes of elements in the sha1 portion of a cluster */
#define WII_H0_LOC 0
#define WII_H0_SIZE 0x26C
#define WII_H1_LOC 0x280
#define WII_H1_SIZE 160
#define WII_H2_LOC 0x340
#define WII_H2_SIZE 160

/* Generate H2 for a group, assumes H1 and H0 are present */
int generate_h2(uint8_t *pgroup)
{
	int i=0;
	/* Generate H2 in the first cluster of the first subgroup */
	while(i<8)
	{
		oar_sha1(pgroup+(i*WII_SUBGROUP_SIZE)+WII_H1_LOC, WII_H1_SIZE, pgroup+WII_H2_LOC+(i*20));
		++i;
	}
	/* Copy H2 into all of the other clusters */
	i=1;
	while(i<64)
	{
		memcpy(pgroup+(i*WII_CLUSTER_SIZE)+WII_H2_LOC, pgroup+WII_H2_LOC, WII_H2_SIZE);
		++i;
	}
	return 1;
}

/* Generate H1 for a subgroup, assumes H0 is present */
int generate_h1(uint8_t *psubgroup)
{
	int i=0;
	/* Generate H1 in the first cluster of subgroup */
	while(i<8)/* For every cluster in subgroup */
	{
		oar_sha1(psubgroup+(i*WII_CLUSTER_SIZE)+WII_H0_LOC, WII_H0_SIZE, psubgroup+WII_H1_LOC+(i*20));
		++i;
	}
	/* Copy H1 into all of the subgroups */
	i=1;
	while(i<8)
	{
		memcpy(psubgroup+(i*WII_CLUSTER_SIZE)+WII_H1_LOC, psubgroup+WII_H1_LOC, WII_H1_SIZE);
		++i;
	}
	return 1;
}

/* Generate H0 for a cluster, assumes decrypted data is present */
int generate_h0(uint8_t *pcluster)
{
	int i=0;
	while(i<31)
	{
		oar_sha1(pcluster+((i+1)*1024), 1024, pcluster+(i*20));
		++i;
	}
	return 1;
}

/* Generate unencrypted sha1 chunks from decrypted data in wii->decrypted_group */
int generate_sha1_chunks(struct wii *wii)
{
	int i=0;
	assert(wii!=NULL);
	/* Generate H0 for each cluster */
	while(i<64)
	{
		generate_h0(wii->decrypted_group+(i*WII_CLUSTER_SIZE));
		++i;
	}

	/* Generate H1 for each subgroup */
	i=0;
	while(i<8)
	{
		generate_h1(wii->decrypted_group+(i*WII_SUBGROUP_SIZE));
		++i;
	}

	/* generate H2 for the group */
	generate_h2(wii->decrypted_group);
	return 1;
}

/* Take wii->decrypted_group and fully generate the encrypted form */
int encode_group(struct wii *wii)
{
	int i=0;
	uint8_t zero[16]={0};
	assert(wii!=NULL);
	/* Generate sha1 chunks */
	generate_sha1_chunks(wii);
	while(i<64)/* for every cluster in group */
	{
		/* Encrypt sha1 chunk */
		oar_aes128cbc_encrypt(wii->titlekey, zero, wii->encrypted_group+(i*WII_CLUSTER_SIZE), wii->decrypted_group+(i*WII_CLUSTER_SIZE), 1024);
		/* Encrypt data */
		oar_aes128cbc_encrypt(wii->titlekey, wii->encrypted_group+(i*WII_CLUSTER_SIZE)+0x3D0, wii->encrypted_group+(i*WII_CLUSTER_SIZE)+1024, wii->decrypted_group+(i*WII_CLUSTER_SIZE)+1024, WII_CLUSTER_SIZE-1024);
		++i;
	}
	return 1;
}

/* Decrypts data from an encrypted cluster */
int decrypt_clusterdata(uint8_t *ccluster, uint8_t *pcluster, uint8_t *titlekey)
{
	oar_aes128cbc_decrypt(titlekey, ccluster+0x3D0, ccluster+1024, pcluster+1024, WII_CLUSTER_SIZE-1024);
	return 1;
}

/* Decode a group, aka decrypt data chunks and zero sha1 chunks */
int decode_group(struct wii *wii)
{
	int i=0;
	assert(wii!=NULL);
	while(i<64)/* Decrypt data */
	{
		decrypt_clusterdata(wii->encrypted_group+(i*WII_CLUSTER_SIZE), wii->decrypted_group+(i*WII_CLUSTER_SIZE), wii->titlekey); /* decrypt data portion */
		memset(wii->decrypted_group+(i*WII_CLUSTER_SIZE), 0, 1024);/* zero sha1 portion */
		++i;
	}
	return 1;
}

/* Decode group and re-encode it to check that it can be modelled */
int decode_check_group(struct wii *wii)
{
	uint8_t test[1024];
	uint8_t zero[16]={0};
	int i=0;
	assert(wii!=NULL);

	/* wii->encrypted_group contains a full encrypted group */
	/* Decode it into wii->decrypted_group */

	decode_group(wii);

	/* Regenerate and encrypt sha1 chunks using decrypted data to check if group can be fully modelled */
	generate_sha1_chunks(wii);
	while(i<64)
	{
		oar_aes128cbc_encrypt(wii->titlekey, zero, test, wii->decrypted_group+(i*WII_CLUSTER_SIZE), 1024);
		if(memcmp(test, wii->encrypted_group+(i*WII_CLUSTER_SIZE), 1024)!=0)
			return 0;
		++i;
	}
	return 1;
}

void populate_partition_table(uint8_t *enc, struct wii *wii)
{
	uint64_t encloc=0, i=0;

	assert(enc!=NULL);
	assert(wii!=NULL);
	assert(wii->part_table==NULL);/* Should only call this function when part table uninitialised */

	wii->part_count=get32lsb(enc);
	encloc+=4;

	/* Read partition info */
	wii->part_table=mallocount(sizeof(struct wiipartition)*wii->part_count, NULL, NULL);

	while(i<wii->part_count)
	{
		memcpy(wii->part_table[i].titlekey, enc+encloc, 16);
		encloc+=16;
		wii->part_table[i].offset=get32lsb(enc+encloc);
		encloc+=4;
		wii->part_table[i].fullgroup_count=get32lsb(enc+encloc);
		encloc+=4;
		wii->part_table[i].grouparray=enc+encloc;
		encloc+=(wii->part_table[i].fullgroup_count/8);
		encloc+=(wii->part_table[i].fullgroup_count%8==0?0:1);
		++i;
	}
	wii->padding_array_loc=encloc;
}

/*
	Decode group:
	* Decrypt data using partition key with iv of cluster+0x3D0
	* Decrypt sha1 table using partition key with iv of 0
*/

int read_wii(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe)
{
	uint8_t *sector, *rawgroup;
	uint32_t i, padloc=0;

	int secsize, bufferloc=0;
	uint64_t j=0;
	uint64_t start, end, trimstart, trimend;

	assert(file_header!=NULL);
	assert(dlt!=NULL);
	assert(fin!=NULL);
	assert(usbloc!=NULL);
	assert(buffer!=NULL);

	secsize=file_header->type==FILE_TYPE_BIN?2352:2048;
	readmap(size, offset, secsize, &start, &end, &trimstart, &trimend);

	/* Check for over reading and truncate that puppy */
	if(end>file_header->sector_count)
	{
		end=file_header->sector_count;
		trimend=0;
	}

	sector=mallocount(2048, NULL, NULL);

	wii->partid=0;
	/* Decode image */
	i=0;
	while(i<end)
	{
		if(wii->partid<wii->part_count && wii->part_table[wii->partid].offset==i)/* Detect when we are at a handled encrypted partition */
		{
			memcpy(wii->titlekey, wii->part_table[wii->partid].titlekey, 16);
			wii->groupid=0;
			while(wii->groupid<wii->part_table[wii->partid].fullgroup_count)/* Process every full group in partition */
			{
				/* Read group */
				if(i+(WII_GROUP_SIZE/2048)>=start)/* If we are interested in a sector in this group, process group */
				{
					j=0;
					while(j<(WII_GROUP_SIZE/2048))
					{
						/* Find each sector to populate wii struct with current group */
						dataraw(i, dlt, fin, 2048, usbloc[0], wii->decrypted_group+(j*2048));
						++i;
						++j;
					}
					/* Encode group if decoded */
					if( (wii->part_table[wii->partid].grouparray[wii->groupid/8]>>(wii->groupid%8))%2 )
					{
						/* Group was decoded, encode it */
						encode_group(wii);
						rawgroup=wii->encrypted_group;
					}
					else
					{
						/* Group stored as-is */
						rawgroup=wii->decrypted_group;
					}
					/* Rewind i and iterate over the raw group to find sector we're interested in */
					i-=(WII_GROUP_SIZE/2048);
					j=0;
					while(j<(WII_GROUP_SIZE/2048))
					{
						if(i>=end)
							break;
						if(i>=start && i<end)
							update_buffer(buffer, &bufferloc, i, start, end, trimstart, trimend, secsize, rawgroup+(j*2048));
						++i;
						++j;
					}
				}
				if(i>=end)
					break;
				++wii->groupid;
			}
			++wii->partid;
		}
		else/* Process a non-partition sector */
		{
			if(i>=start)/* If we are interested in this sector */
			{
				/* Do non-group handling aka padding handling, same principle as NGC */
				if( (enc[wii->padding_array_loc+(padloc/8)]>>(padloc%8))%2 )
				{
					/* Data was predicted, find it */
					/* Update junk block if necessary */
					if(nod->curr_blockcount!=(i/128))
						get_junk_block(nod, i/128);
					update_buffer(buffer, &bufferloc, i, start, end, trimstart, trimend, secsize, nod->garbage_block+((i%128)*2048));
				}
				else
				{
					/* Copy data from bucket */
					dataraw(i, dlt, fin, 2048, usbloc[0], sector);
					/* Write to output */
					update_buffer(buffer, &bufferloc, i, start, end, trimstart, trimend, secsize, sector);
				}
			}
			++i;
			++padloc;
		}
	}

	free(sector);
	return bufferloc;
}

void decode_wii(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *usbloc, uint8_t *enc)
{
	uint8_t *sector;
	uint32_t i, j, padloc=0;
	struct nod *nod=0;
	struct wii *wii=0;

	assert(dlt!=NULL);
	assert(enc!=NULL);
	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sha1!=NULL);
	assert(stats!=NULL);
	assert(usbloc!=NULL);

	sector=mallocount(2048, NULL, NULL);
	nod=mallocount(sizeof(struct nod), NULL, NULL);
	wii=mallocount(sizeof(struct wii), NULL, NULL);

	stats->memret=96+2048+sizeof(struct nod)+sizeof(struct wii);

	populate_partition_table(enc, wii);
	stats->memret+=(sizeof(struct wiipartition)*wii->part_count);

	wii->partid=0;
	/* Decode image */
	i=0;
	while(i<file_header->sector_count)
	{
		if(wii->partid<wii->part_count && wii->part_table[wii->partid].offset==i)/* Detect when we are at a handled encrypted partition */
		{
			memcpy(wii->titlekey, wii->part_table[wii->partid].titlekey, 16);
			wii->groupid=0;
			while(wii->groupid<wii->part_table[wii->partid].fullgroup_count)/* Process every full group in partition */
			{
				/* Read group */
				j=0;
				while(j<(WII_GROUP_SIZE/2048))
				{
					/* Find each sector to populate wii struct with current group */
					dataraw(i, dlt, fin, 2048, usbloc[0], wii->decrypted_group+(j*2048));
					++i;
					++j;
				}
				/* Encode group if decoded */
				if( (wii->part_table[wii->partid].grouparray[wii->groupid/8]>>(wii->groupid%8))%2 )
				{
					/* Group was decoded, encode it */
					encode_group(wii);
					read_hash_write(NULL, fout, sha1, NULL, wii->encrypted_group, WII_GROUP_SIZE);
				}
				else
				{
					/* Group stored as-is */
					read_hash_write(NULL, fout, sha1, NULL, wii->decrypted_group, WII_GROUP_SIZE);
				}
				++wii->groupid;
			}
			++wii->partid;
		}
		else/* Process a non-partition sector */
		{
			/* Do non-group handling aka padding handling, same principle as NGC */
			if( (enc[wii->padding_array_loc+(padloc/8)]>>(padloc%8))%2 )
			{
				/* Data was predicted, find it */
				/* Update junk block if necessary */
				if(nod->curr_blockcount!=(i/128))
					get_junk_block(nod, i/128);
				read_hash_write(NULL, fout, sha1, NULL, nod->garbage_block+((i%128)*2048), 2048);
			}
			else
			{
				/* Copy data from bucket */
				dataraw(i, dlt, fin, 2048, usbloc[0], sector);
				/* Hash, write to output */
				read_hash_write(NULL, fout, sha1, NULL, sector, 2048);
				if(i==0)
				{
					/* First sector, intialise padding modelling */
					memcpy(nod->id, sector, 4);
					nod->disc_number=sector[6];
					get_junk_block(nod, 0);
				}
			}
			++i;
			++padloc;
		}
	}

	free(nod);
	free(sector);
	free(wii->part_table);
	free(wii);
}

/** Takes an input stream and determines if it's a WII image */
int is_wii(struct file_header *file_header, FILE *fin)
{
	int64_t seek=0;
	uint8_t *sector;
	assert(fin!=NULL);

	sector=mallocount(2048, NULL, NULL);

	fseekass(fin, 0, SEEK_END);
	seek=ftellass(fin);
	seek/=2048;
	/* Check that the image is one of the two sizes a Wii image can be */
	if(seek!=WII_DL_SECTOR_COUNT && seek!=WII_SL_SECTOR_COUNT)
	{
		free(sector);
		return 0;
	}

	fseekass(fin, 0, SEEK_SET);
	freadass(sector, 1, 2048, fin);
	/* Check the header for the Wii magic word */
	if(nod_read_header(sector)==FILE_TYPE_WII)
	{
		free(sector);
		return 1;
	}
	free(sector);
	return 0;
}

/* Decrypt title key in ticket using a common key in wii struct */
int get_titlekey(uint8_t *ticket, struct wii *wii)
{
	uint8_t *ckey=0;
	uint8_t iv[16];
	assert(ticket!=NULL);
	assert(wii!=NULL);
	if(ticket[0x01F1]==0)
		ckey=wii->commonkey;
	else if(ticket[0x01F1]==1)
		ckey=wii->koreacommonkey;
	else
		return oar_abort(" Error unknown common key index in ticket, accidentally parsing a Wii U image?\n");
	memset(iv, 0, 16);
	memcpy(iv, ticket+0x1DC, 8);
	oar_aes128cbc_decrypt(ckey, iv, ticket+0x1BF, wii->titlekey, 16);
	return 1;
}

/**
 * Read basic partition information to determine if it's worth modelling this partition.
 *
 * Reads data into partition struct if provided and the partition is worth modelling
 * Returns 1 for worth modelling, 0 otherwise
 */
int read_partition(FILE *fin, uint64_t off, struct wii *wii, struct wiipartition *partition)
{
	uint8_t ticket[0x2A4];
	uint8_t scratch[28];
	uint64_t data_offset, data_size, groups;
	assert(fin!=NULL);
	assert(wii!=NULL);

	fseekass(fin, off, SEEK_SET);
	freadass(ticket, 1, 0x2A4, fin);
	get_titlekey(ticket, wii);
	freadass(scratch, 1, 28, fin);
	data_offset=4;
	data_offset*=get32msb(scratch+20);
	data_size=4;
	data_size*=get32msb(scratch+24);
	groups=data_size/WII_GROUP_SIZE;

	/* If there are no full groups in this partition, don't bother populating partition info */
	if(groups==0)
		return 0;

	/* If we have been passed a partition struct to get return data, populate it */
	if(partition)
	{
		memcpy(partition->titlekey, wii->titlekey, 16);
		partition->offset=((off+data_offset)/2048);
		partition->fullgroup_count=groups;
	}
	return 1;
}

/* read a Wii image for encrypted partition data
 * If partitions is passed, read partition data into it
 * Otherwise, count partitions into partition_count
 */
int getpartitionlayout(FILE *fin, struct wii *wii, struct wiipartition *partitions, uint32_t *partition_count)
{
	uint8_t scratch[32];
	uint32_t partitiontable_count[4];
	uint64_t partitiontable_offset[4];
	uint64_t *partition_offset;
	uint32_t partcount=0, partloc, worthycount=0;
	uint64_t seek;
	int i=0, j=0;
	struct wiipartition *currpartition=0;
	assert(fin!=NULL);
	assert(wii!=NULL);

	if(partitions)
		currpartition=mallocount(sizeof(struct wiipartition), NULL, NULL);

	/* Read partition info tables table to find partition info tables */
	fseekass(fin, 0x40000, SEEK_SET);
	freadass(scratch, 1, 32, fin);
	while(i<4)
	{
		partitiontable_count[i]=get32msb(scratch+(i*8));
		partcount+=partitiontable_count[i];
		partitiontable_offset[i]=4;
		partitiontable_offset[i]*=get32msb(scratch+(i*8)+4);
		++i;
	}

	/* Read partition info tables to find partition offsets */
	partition_offset=mallocount(sizeof(uint64_t)*partcount, NULL, NULL);
	partloc=0;
	i=0;
	while(i<4)
	{
		j=0;
		while(j<partitiontable_count[i])
		{
			seek=partitiontable_offset[i];
			seek+=(j*8);
			fseekass(fin, seek, SEEK_SET);
			freadass(scratch, 1, 4, fin);
			partition_offset[partloc]=4;
			partition_offset[partloc]*=get32msb(scratch);
			++partloc;
			++j;
		}
		++i;
	}

	/* for every partition, read it and see if it's worth modelling */
	partloc=0;
	while(partloc<partcount)
	{
		if(read_partition(fin, partition_offset[partloc], wii, currpartition))
		{
			/* Partition is worthy of modelling */
			if(partitions)/* If a partition array has been passed to populate */
				memcpy(partitions+worthycount, currpartition, sizeof(struct wiipartition));
			++worthycount;
		}
		++partloc;
	}

	if(partitions)/* Sort partitions into order in image */
		qsort(partitions, worthycount, sizeof(struct wiipartition), wiipartition_compare);

	if(!partitions)/* If we are just counting worthy partitions */
		partition_count[0]=worthycount;

	/* Bounds and overlap checks TODO */
	if(currpartition)
		free(currpartition);
	free(partition_offset);
	return 1;
}

/*
	Wii encoding format:
	* uint32_t num partitions
	* For each partition:
		* 16 byte title key
		* uint32_t Offset to first group in sectors
		* uint32_t Number of full groups in partition
		* Bit array for full groups in partition showing:
			* 0 for stored as-is, 1 for stored fully decoded (decrypted and sha1 chunk zeroed)
	* Bit array for all remaining sectors (sectors not covered by full groups)
		* 0 for stored as-is, 1 for padding can be predicted and stored zeroed
 */

/* Read WII file sector by sector, updating dlt and writing unique sectors for a particular bucket */
void gather_wii(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype)
{
	uint8_t digest[20];
	uint8_t *sector;
	uint8_t *zero;
	uint8_t zerohash[20];
	uint64_t i=0, j=0, seek;
	uint64_t grouploc=0, partloc=0;
	int modpad=0, modgroup=0;

	struct wii *wii=0;

	struct wiipartition *partition=0;
	uint32_t partition_count;

	struct sector keysector;
	uint8_t *group_ptr;

	/* Early exit for sector sizes that cannot be stored in an ISO image */
	if(bucketsize!=2048)
		return;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(sectortable!=NULL);
	assert(sha256!=NULL);
	assert(dlt!=NULL);
	assert(bucketindex!=NULL);
	assert(bucket!=NULL);
	assert(enctype!=NULL);
	assert(stats!=NULL);

	sector=mallocount(2048, NULL, NULL);
	zero=mallocount(2048, NULL, NULL);
	wii=mallocount(sizeof(struct wii), NULL, NULL);

	stats->memret=4096+sizeof(struct wii)+100;

	memcpy(wii->commonkey, ops->wiicommonkey, 16);
	memcpy(wii->koreacommonkey, ops->wiikoreancommonkey, 16);

	oar_sha1(zero, 2048, zerohash);

	/* Recreate encoding elements not encapsulated by enctype, simply the partition layout */
	/* Read partition metadata ahead of time to determine size/layout of encoding */
	getpartitionlayout(fin, wii, NULL, &partition_count);
	partition=mallocount(partition_count*sizeof(struct wiipartition), NULL, NULL);
	getpartitionlayout(fin, wii, partition, &partition_count);

	/* Process image */
	rewind(fin);
	i=0;
	while(i<file_header->sector_count)
	{
		if(partloc<partition_count && partition[partloc].offset==i)/* Detect when we are at a handled encrypted partition */
		{
			memcpy(wii->titlekey, partition[partloc].titlekey, 16);
			grouploc=0;
			while(grouploc<partition[partloc].fullgroup_count)/* Process every full group in partition */
			{
				/* Read group into struct */
				seek=i;
				seek*=2048;
				fseekass(fin, seek, SEEK_SET);
				freadass(wii->encrypted_group, 1, WII_GROUP_SIZE, fin);

				/* Use decoded form of group if modellable */
				if((enctype[i/8]>>(i%8))%2)/* Process function has already determined if group is modellable */
				{
					/* Group modelled */
					decode_group(wii);
					group_ptr=wii->decrypted_group;
					++modgroup;
				}
				else
				{
					group_ptr=wii->encrypted_group;
				}
				/* Update DLT with group data, encrypted or decrypted depending on what group_ptr points to */
				j=0;
				while(j<(WII_GROUP_SIZE/2048))
				{
					oar_sha1(group_ptr+(j*2048), 2048, digest);
					/* Search for sector in sectortable to build dlt entry */
					memcpy(keysector.hash, digest, 12);
					keysector.id=global_position+i;
					/* updatedlt */
					if(updatedlt(dlt, &keysector, sectortable, sectortable_len, bucketindex[0]))
					{
						/* unique sector, write it to bucket etc */
						bucketindex[0]++;
						read_hash_write(NULL, bucket, NULL, sha256, group_ptr+(j*2048), 2048);
					}
					else
					{
						if(((enctype[i/8]>>(i%8))%2)==0)
							stats->running_bytes_saved_by_dedupe+=2048;
					}
					++i;
					++j;
				}
				++grouploc;
			}
			++partloc;
		}
		else/* Process a non-partition sector */
		{
			if((enctype[i/8]>>(i%8))%2)
			{
				/* Predictable through padding modelling */
				memset(sector, 0, 2048);
				memcpy(digest, zerohash, 20);
				++modpad;
			}
			else
			{
				/* Unpredictable, treat as ISO sector */
				seek=i;
				seek*=2048;
				fseekass(fin, seek, SEEK_SET);
				freadass(sector, 1, 2048, fin);
				oar_sha1(sector, 2048, digest);
			}

			/* Search for sector in sectortable to build dlt entry */
			memcpy(keysector.hash, digest, 12);
			keysector.id=global_position+i;
			/* updatedlt */
			if(updatedlt(dlt, &keysector, sectortable, sectortable_len, bucketindex[0]))
			{
				/* unique sector, write it to bucket etc */
				bucketindex[0]++;
				read_hash_write(NULL, bucket, NULL, sha256, sector, 2048);
			}
			else
			{
				if(((enctype[i/8]>>(i%8))%2)==0)
					stats->running_bytes_saved_by_dedupe+=2048;
			}
			++i;
		}
	}

	free(partition);
	free(wii);

	/* Update peak memory stat */
	mempeak_update(&stats->memcurr, &stats->mempeak, stats->memret);

	/* As no more gather ops need to be done, free enctype and reflect that in memory stats */
	free(enctype);
	stats->memcurr-=((file_header->sector_count/8)+1);
}

/*
	Read a WII file, processing sectors by hashing their decoded form and adding it to a sectortable
 */
uint8_t* process_wii(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256)
{
	uint32_t i=0, j=0;
	uint8_t digest[20];
	uint8_t *sector;
	uint8_t *zero;
	uint8_t zerohash[20];
	uint8_t *enc;
	uint64_t enclen=0, encsize;
	uint64_t paddingenc_len;

	uint8_t *padding_encoding=0;
	uint64_t padding_encoding_loc=0, grouploc=0, partloc=0;
	int modpad=0, modgroup=0;

	struct nod *nod=0;
	struct wii *wii=0;

	struct wiipartition *partition=0;
	uint32_t partition_count;
	uint8_t *enctype=0;

	assert(file_header!=NULL);
	assert(fin!=NULL);
	assert(foutencode!=NULL);
	assert(sectortable!=NULL);
	assert(enctype!=NULL);
	assert(sha1!=NULL);
	assert(sha256!=NULL);

	enctype=mallocount((file_header->sector_count/8)+1, &stats->memcurr, &stats->mempeak);

	nod=mallocount(sizeof(struct nod), NULL, NULL);
	wii=mallocount(sizeof(struct wii), NULL, NULL);
	sector=mallocount(2048, NULL, NULL);
	zero=mallocount(2048, NULL, NULL);

	if(memcmp(ops->wiicommonkey, zero, 16)==0 || memcmp(ops->wiikoreancommonkey, zero, 16)==0)
	{
		printf("Error: User attempting to encode WII images without supplying the common keys, abort\n");
		exit(1);
	}

	oar_sha1(zero, 2048, zerohash);

	memcpy(wii->commonkey, ops->wiicommonkey, 16);
	memcpy(wii->koreacommonkey, ops->wiikoreancommonkey, 16);

	/* Read partition metadata ahead of time to determine size/layout of encoding */
	getpartitionlayout(fin, wii, NULL, &partition_count);
	partition=mallocount(partition_count*sizeof(struct wiipartition), NULL, NULL);
	getpartitionlayout(fin, wii, partition, &partition_count);

	stats->memret=4096+sizeof(struct nod)+sizeof(struct wii)+128+(partition_count*sizeof(struct wiipartition));

	/* Determine size of encoding */
	paddingenc_len=file_header->sector_count;/* Work out size of padding encoding by starting with all sectors being encoded and removing group encoding as they're encountered */
	enclen+=4;/* partition count */
	i=0;
	while(i<partition_count)
	{
		enclen+=24;/* title key, sector offset, group count */
		/* group encoding */
		enclen+=(partition[i].fullgroup_count/8);
		if(partition[i].fullgroup_count%8)
			++enclen;
		paddingenc_len-=(partition[i].fullgroup_count*64*16);
		++i;
	}
	/* padding encoding */
	enclen+=(paddingenc_len/8);
	if(paddingenc_len%8)
		++enclen;

	enc=mallocount(enclen, NULL, NULL);
	encsize=enclen;

	/* Write partition info to encoding and set pointers to group encodings and padding encoding */
	enclen=0;
	put32lsb(enc+enclen, partition_count);enclen+=4;
	i=0;
	while(i<partition_count)
	{
		memcpy(enc+enclen, partition[i].titlekey, 16);enclen+=16;/* titlekey */
		put32lsb(enc+enclen, partition[i].offset);enclen+=4;/* sector offset */
		put32lsb(enc+enclen, partition[i].fullgroup_count);enclen+=4;/* fullgroup count */
		/* Set and move past group encoding */
		partition[i].grouparray=enc+enclen;
		enclen+=(partition[i].fullgroup_count/8);
		if(partition[i].fullgroup_count%8)
			++enclen;
		++i;
	}
	padding_encoding=enc+enclen;

	/* Process image */
	rewind(fin);
	i=0;
	while(i<file_header->sector_count)
	{
		if(partloc<partition_count && partition[partloc].offset==i)/* Detect when we are at a handled encrypted partition */
		{
			memcpy(wii->titlekey, partition[partloc].titlekey, 16);
			grouploc=0;
			while(grouploc<partition[partloc].fullgroup_count)/* Process every full group in partition */
			{
				/* Read encrypted group into struct */
				read_hash_write(fin, NULL, sha1, NULL, wii->encrypted_group, WII_GROUP_SIZE);
				/* Encode group if decoded */
				if(decode_check_group(wii))
				{
					/* Group modellable */
					++modgroup;
					enctype[i/8]+=1<<(i%8);/* Mark group as modellable in enctype array */
					partition[partloc].grouparray[grouploc/8]+=1<<(grouploc%8);/* Mark group as modellable in group array */
					decode_group(wii);/* decode again without generating sha1 chunks */
					/* Add decoded sectors to sectortable */
					j=0;
					while(j<(WII_GROUP_SIZE/2048))
					{
						oar_sha1(wii->decrypted_group+(j*2048), 2048, digest);
						memcpy(sectortable[i].hash, digest, 12);
						sectortable[i].id=global_position+i;
						++i;
						++j;
					}
					stats->running_bytes_saved_by_modelling+=65536;/* The sha1 chunks that are now zeroed */
					stats->running_bytes_decrypted_by_modelling+=(WII_GROUP_SIZE-65536);/* The decrypted data section */
				}
				else
				{
					/* Group not modellable */
					/* Add encoded sectors to sectortable */
					j=0;
					while(j<(WII_GROUP_SIZE/2048))
					{
						oar_sha1(wii->encrypted_group+(j*2048), 2048, digest);
						memcpy(sectortable[i].hash, digest, 12);
						sectortable[i].id=global_position+i;
						++i;
						++j;
					}
				}
				++grouploc;
			}
			++partloc;
		}
		else/* Process a non-partition sector */
		{
			/* Do non-group handling aka padding handling, same principle as NGC */
			/* Read + image hash */
			read_hash_write(fin, NULL, sha1, NULL, sector, 2048);

			/* Determine if sector is predictable */
			if(i==0)
			{
				/* First sector, seed padding algorithm with required data and ensure no invalid data is present for safety */
				memcpy(nod->id, sector, 4);
				nod->disc_number=sector[6];
				get_junk_block(nod, 0);
			}
			if(nod->curr_blockcount!=(i/128))
				get_junk_block(nod, i/128);/* Calculate next junk block if necessary */
			if(memcmp(sector, nod->garbage_block+(2048*(i%128)), 2048)==0)
			{
				/* Predicted, add zeroed sector to sectortable and set encoding accordingly */
				++modpad;
				enctype[i/8]+=1<<(i%8);
				padding_encoding[padding_encoding_loc/8]+=1<<(padding_encoding_loc%8);
				memcpy(digest, zerohash, 20);
				stats->running_bytes_saved_by_modelling+=2048;
			}
			else
			{
				/* sector hash */
				oar_sha1(sector, 2048, digest);
			}
			++padding_encoding_loc;

			/* add to sector table */
			memcpy(sectortable[i].hash, digest, 12);
			sectortable[i].id=global_position+i;
			++i;
		}
	}

	/* Finalise anything that needs finalising */
	fclose(fin);
	oar_sha1_final(file_header->sha1, sha1);
	/* Write encoding to file */
	read_hash_write(NULL, foutencode, NULL, sha256, enc, encsize);
	file_header->enc_len=encsize;

	free(enc);
	free(nod);
	free(partition);
	free(wii);
	free(sector);
	free(zero);

	return enctype;
}
