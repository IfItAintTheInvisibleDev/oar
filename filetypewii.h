/**
 * @file Define functions for handling WII images
 */

#ifndef OAR_FILETYPEWII
#define OAR_FILETYPEWII

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cryptowrap.h"
#include "common.h"
#include "filetype.h"
#include "headers.h"
#include "nod.h"

#define WII_SL_SECTOR_COUNT 2294912
#define WII_DL_SECTOR_COUNT 4155840

/* Parse enc to populate partition table */
void populate_partition_table(uint8_t *enc, struct wii *wii);

int read_wii(struct file_header *file_header, uint8_t *dlt, uint8_t *enc, FILE *fin, uint64_t *usbloc, char *buffer, size_t size, off_t offset, struct nod *nod, struct ps3 *ps3, struct wii *wii, struct ybe *ybe);

void decode_wii(struct file_header *file_header, struct stats *stats, FILE *fin, FILE *fout, oar_sha1_ctx *sha1, uint8_t *dlt, uint64_t *usbloc, uint8_t *enc);

/** Takes an input stream and determines if it's a WII image */
int is_wii(struct file_header *file_header, FILE *fin);

/** Read WII image, updating DLT and writing unique sectors for a particular bucket */
void gather_wii(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, struct sector *sectortable, uint32_t sectortable_len, uint32_t global_position, oar_sha256_ctx *sha256, uint8_t *dlt, uint32_t *bucketindex, FILE *bucket, uint32_t bucketsize, uint8_t *enctype);

/** Read WII image to process sectors into sectortable */
uint8_t* process_wii(struct file_header *file_header, struct stats *stats, struct oar_options *ops, FILE *fin, FILE *foutencode, FILE *supplemental, struct sector *sectortable, uint32_t global_position, oar_sha1_ctx *sha1, oar_sha256_ctx *sha256);

#ifdef __cplusplus
}
#endif

#endif
