#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ybe.h"
#include "common.h"

#include <sys/stat.h>

extern uint8_t scrambled_xor_table[2352];

int gen_cooked(uint8_t *sector)
{
	int i=0;
	while(i<2048)
	{
		sector[i]=rand()%256;
		++i;
	}
}

int gen_uncooked(uint8_t *sector, struct ybe *y, int type, int include_unpredictable, int include_scrambled)
{
	int i, numerrs;

	/* Fill sector with random data to seed data */
	i=0;
	while(i<2352)
	{
		y->data[i]=rand()%256;
		++i;
	}

	/* Generate valid fully predictable sector */
	switch(type)
	{
		case 0:/*M0*/
			y->enc[0]=(YBE_FLAG_TYPE_OTHER)+(YBE_FLAG_SUBTYPE_M0<<2);
			break;
		case 1:/*M1*/
			y->enc[0]=(YBE_FLAG_TYPE_M1);
			break;
		case 2:/*M2*/
			y->enc[0]=(YBE_FLAG_TYPE_OTHER)+(YBE_FLAG_SUBTYPE_M2<<2);
			break;
		case 3:/*M2F1*/
			y->enc[0]=(YBE_FLAG_TYPE_M2F1);
			break;
		case 4:/*M2F2*/
			y->enc[0]=(YBE_FLAG_TYPE_M2F2);
			break;
		case 5:/*raw*/
			y->enc[0]=(YBE_FLAG_TYPE_OTHER)+(YBE_FLAG_SUBTYPE_RAW<<2);
			break;
		case 6:/* random */
			switch(rand()%6)
			{
				case 0:/*M0*/
					y->enc[0]=(YBE_FLAG_TYPE_OTHER)+(YBE_FLAG_SUBTYPE_M0<<2);
					break;
				case 1:/*M1*/
					y->enc[0]=(YBE_FLAG_TYPE_M1);
					break;
				case 2:/*M2*/
					y->enc[0]=(YBE_FLAG_TYPE_OTHER)+(YBE_FLAG_SUBTYPE_M2<<2);
					break;
				case 3:/*M2F1*/
					y->enc[0]=(YBE_FLAG_TYPE_M2F1);
					break;
				case 4:/*M2F2*/
					y->enc[0]=(YBE_FLAG_TYPE_M2F2);
					break;
				case 5:/*raw*/
					y->enc[0]=(YBE_FLAG_TYPE_OTHER)+(YBE_FLAG_SUBTYPE_RAW<<2);
					break;
			}
			break;
	}

	if(!decode_sector(y))
	{
		printf("Failed to decode sector:\n");
		return 7;
	}

	/* Pepper sector with random bytes to simulate unpredictable elements */
	if(include_unpredictable)
	{
		numerrs=rand()%10;
		i=0;
		while(i<numerrs)
		{
			y->sector[rand()%2352]=rand()%256;
			++i;
		}
	}

	/* Scramble some sectors */
	if(include_scrambled)
	{
		if(rand()%100==0)
		{
			i=0;
			while(i<2352)
			{
				y->sector[i]=y->sector[i]^scrambled_xor_table[i];
				++i;
			}
		}
	}

	memcpy(sector, y->sector, 2352);
}

struct tests
{
	char *name;
	int type;
	int unpredictable;
	int scrambled;
	int torture;
	int iso;
	int bin;
};

int main()
{
	int i, j, k, count;

	FILE *fout=0;
	struct ybe *ybe=0;

	struct tests tests[]=
	{
		{"M0", 0, 0, 0, 0, 0, 1},
		{"M1", 1, 0, 0, 0, 0, 1},
		{"M2", 2, 0, 0, 0, 0, 1},
		{"M2F1", 3, 0, 0, 0, 0, 1},
		{"M2F2", 4, 0, 0, 0, 0, 1},
		{"RAW", 5, 0, 0, 0, 0, 1},
		{"Mix", 6, 0, 0, 0, 0, 1},
		{"MixUnpredictable", 6, 1, 0, 0, 0, 1},
		{"MixUnpredictableScrambled", 6, 1, 1, 0, 0, 1},

		{"ISO", 6, 1, 1, 0, 1, 0},
		{"ISOBIN", 6, 1, 1, 0, 1, 1},
		/* {"Torture", 6, 1, 1, 1, 1, 1}, */
		{NULL, 0, 0, 0, 0},
	};

	uint8_t data[2352];
	uint8_t dupe[2352];

	char outname[128];
	int numimg=6;

	srand(time(NULL));

	ybe=malloc(sizeof(struct ybe));
	ybe_init(ybe);

	printf("genbin generating test images, this may take some time\n");
	k=0;
	while(tests[k].name)
	{
		printf("Test %s\n", tests[k].name);
		sprintf(outname, "%s", tests[k].name);
		mkdir(outname, 0777);

		count = tests[k].torture?50000:5000;

		if(tests[k].bin)
		{
			gen_uncooked(dupe, ybe, tests[k].type, tests[k].unpredictable, tests[k].scrambled);

			j=0;
			while(j<numimg)
			{
				sprintf(outname, "%s/b%d.bin", tests[k].name, j);
				fout=fopen(outname, "wb");

				i=0;
				while(i<count)
				{
					gen_uncooked(data, ybe, tests[k].type, tests[k].unpredictable, tests[k].scrambled);
					fwrite(data, 1, 2352, fout);
					if(rand()%10==0)/* 10% of the time write a duplicate sector */
					{
						fwrite(dupe, 1, 2352, fout);
					}
					else if(rand()%100==0)/* 1% of the time change the sector to duplicate */
						memcpy(dupe, data, 2352);
					++i;
				}
				fclose(fout);
				++j;
			}
		}

		if(tests[k].iso)
		{
			j=0;
			while(j<numimg)
			{
				sprintf(outname, "%s/b%d.iso", tests[k].name, j);
				fout=fopen(outname, "wb");

				i=0;
				while(i<count)
				{
					gen_cooked(data);
					fwrite(data, 1, 2048, fout);
					if(rand()%10==0)/* 10% of the time write a duplicate sector */
					{
						fwrite(dupe, 1, 2048, fout);
					}
					else if(rand()%100==0)/* 1% of the time change the sector to duplicate */
						memcpy(dupe, data, 2048);

					++i;
				}
				fclose(fout);
				++j;
			}
		}

		++k;
	}

	fclose(fout);

	free(ybe);

}

