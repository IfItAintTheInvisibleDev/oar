#include "headers.h"
#include "common.h"
#include "filetypengc.h"

int free_oar_header(struct oar_header *oar)
{
	int i=0;

	assert(oar!=NULL);

	if(oar->file_headers)
	{
		i=0;
		while(i<oar->filecount)
		{
			if(oar->file_headers[i].filename)
				free(oar->file_headers[i].filename);
			if(oar->file_headers[i].sectors)
				free(oar->file_headers[i].sectors);
			if(oar->file_headers[i].ybe_data)
				free(oar->file_headers[i].ybe_data);
			++i;
		}
	}

	free(oar);

	return 1;
}

uint64_t get_enc_size(struct oar_header *oar)
{
	uint64_t i=0, ret=0;
	assert(oar!=NULL);
	while(i<oar->filecount)
	{
		ret+=oar->file_headers[i].enc_len;
		++i;
	}
	return ret;
}

uint64_t get_header_size(struct oar_header *oar)
{
	uint64_t ret=0;
	assert(oar!=NULL);
	ret=oar->filecount;
	ret*=FILE_HEADER_LEN;
	ret+=OAR_HEADER_LEN;
	return ret;
}

uint64_t get_calculated_filesize(struct oar_header *oar)
{
	uint64_t ret=0;
	assert(oar!=NULL);
	ret=oar->filecount;
	ret*=FILE_HEADER_LEN;
	ret+=OAR_HEADER_LEN;
	return ret;
}

int init_oar_header(struct oar_header *oar)
{
	char str_builder[FILENAME_STRING_LEN];
	assert(oar!=NULL);
	memset(oar, 0, sizeof(struct oar_header));
	sprintf(str_builder, "OAR");
	memcpy(oar->magic, str_builder, 4);

	oar->program_version=7;/* Can handle BIN/ISO/NGC/PS3/WII and account for post-ENC padding */

	return 1;
}

void print_header(struct oar_header *oar)
{
	int i=0;
	assert(oar!=NULL);
	printf("files in archive: %" PRIu64 "\n", oar->filecount);
	printf("Archive filesize: %" PRIu64 "\n", (oar->oar_filesize+OAR_FOOTER_LEN));
	printf("enc data size: %" PRIu64 "\n", get_enc_size(oar));
	while(i<4)
	{
		printf("bucket%d sector count: %" PRIu32 "\n", i, oar->bucket_sector_count[i]);
		++i;
	}
}

void fread_file_headers(struct oar_header *oar, oar_sha256_ctx *sha256, FILE *fin, uint64_t *currloc)
{
	uint8_t scratch[FILE_HEADER_LEN];
	int i=0;

	assert(oar!=NULL);
	assert(fin!=NULL);
	assert(currloc!=NULL);

	oar->file_headers = mallocount(oar->filecount*sizeof(struct file_header), NULL, NULL);

	/* Read file header array from file and count total sectors */
	oar->total_sectors=0;
	i=0;
	while(i<oar->filecount)
	{
		read_hash_write(fin, NULL, NULL, sha256, scratch, FILE_HEADER_LEN);
		oar->file_headers[i].type=get32lsb(scratch+0);
		oar->file_headers[i].sector_count=get64lsb(scratch+4);
		oar->file_headers[i].enc_len=get64lsb(scratch+12);
		memcpy(oar->file_headers[i].sha1, scratch+20, 20);
		oar->total_sectors+=oar->file_headers[i].sector_count;
		currloc[0]+=FILE_HEADER_LEN;
		++i;
	}
}

void fread_header(struct oar_header *oar, oar_sha256_ctx *sha256, FILE *fin, uint64_t *currloc)
{
	uint8_t scratch[OAR_HEADER_LEN];
	int i=0;

	assert(oar!=NULL);
	assert(fin!=NULL);
	assert(currloc!=NULL);

	read_hash_write(fin, NULL, NULL, sha256, scratch, OAR_HEADER_LEN);
	memcpy(scratch+i, oar->magic, 4);i+=4;
	oar->version=get32lsb(scratch+i);i+=4;
	oar->oar_filesize=get64lsb(scratch+i);i+=8;
	oar->filecount=get64lsb(scratch+i);i+=8;
	oar->bucket_sector_count[0]=get32lsb(scratch+i);i+=4;
	oar->bucket_sector_count[1]=get32lsb(scratch+i);i+=4;
	oar->bucket_sector_count[2]=get32lsb(scratch+i);i+=4;
	oar->bucket_sector_count[3]=get32lsb(scratch+i);
	currloc[0]+=OAR_HEADER_LEN;
}

int write_file_header(struct file_header *fh, FILE *fout, oar_sha256_ctx *sha256)
{
	uint8_t scratch[8];
	assert(fh!=NULL);
	put32lsb(scratch, fh->type);
	read_hash_write(NULL, fout, NULL, sha256, scratch, 4);

	put64lsb(scratch, fh->sector_count);
	read_hash_write(NULL, fout, NULL, sha256, scratch, 8);

	put64lsb(scratch, fh->enc_len);
	read_hash_write(NULL, fout, NULL, sha256, scratch, 8);

	read_hash_write(NULL, fout, NULL, sha256, fh->sha1, 20);

	return 1;
}

int write_oar_header(struct oar_header *oar, FILE *fout, oar_sha256_ctx *sha256)
{
	int i;
	char *oarstr="OAR";
	uint8_t scratch[8];
	assert(oar!=NULL);

	read_hash_write(NULL, fout, NULL, sha256, oarstr, 4);

	put32lsb(scratch, oar->version);
	read_hash_write(NULL, fout, NULL, sha256, scratch, 4);

	put64lsb(scratch, oar->oar_filesize);
	read_hash_write(NULL, fout, NULL, sha256, scratch, 8);

	put64lsb(scratch, oar->filecount);
	read_hash_write(NULL, fout, NULL, sha256, scratch, 8);

	i=0;
	while(i<4)
	{
		put32lsb(scratch, oar->bucket_sector_count[i]);
		read_hash_write(NULL, fout, NULL, sha256, scratch, 4);
		++i;
	}

	return 1;
}

int read_titfile(FILE *fin, struct file_header *file_headers, int filecount)
{
	int i=0;
	while(i<filecount)
	{
		file_headers[i].filename=mallocount(FILENAME_STRING_LEN, NULL, NULL);
		if(fgets(file_headers[i].filename, FILENAME_STRING_LEN-1, fin)==NULL) return oar_abort("Error: fgets() failed, abort\n");
		if(strstr(file_headers[i].filename, "\n")==NULL) return oar_abort("Error: strstr() failed to find trailing newline when parsing .tit file, abort\n");
		strstr(file_headers[i].filename, "\n")[0]=0;
		file_headers[i].filename=reallocount(file_headers[i].filename, strlen(file_headers[i].filename)+1, NULL, NULL, 0);
		++i;
	}
	return 1;
}

int write_titfile(FILE *fout, struct file_header *file_headers, int filecount)
{
	int i=0;
	char newline='\n';
	char *nameuse=0;
	while(i<filecount)
	{
		nameuse=file_headers[i].filename;
		if(strrchr(nameuse, '/')!=NULL)
			nameuse=strrchr(file_headers[i].filename, '/')+1;
		fwriteass(nameuse, 1, strlen(nameuse), fout);
		fwriteass(&newline, 1, 1, fout);
		++i;
	}
	return 1;
}

/* Parse info about an archive into oar struct */
void load_archive(struct oar_header *oar, FILE *archive, FILE *titles, oar_sha256_ctx *sha256)
{
	uint64_t currloc=0, tmp, i;
	uint64_t bucket[4]={2048, 2324, 2336, 2352};

	assert(oar!=NULL);
	assert(archive!=NULL);

	rewind(archive);
	fread_header(oar, sha256, archive, &currloc);
	fread_file_headers(oar, sha256, archive, &currloc);

	/* Step through DLT */
	i=0;
	while(i<oar->filecount)
	{
		oar->file_headers[i].dltloc=currloc;
		currloc+=(4*oar->file_headers[i].sector_count);
		++i;
	}

	/* Step through ENC */
	i=0;
	while(i<oar->filecount)
	{
		oar->file_headers[i].encloc=currloc;
		currloc+=(oar->file_headers[i].enc_len);
		++i;
	}

	/* Skip PAD */
	oar->padloc=currloc;
	currloc+=((2048-(currloc%2048))%2048);

	/* Step through USB* */
	i=0;
	while(i<4)
	{
		oar->usbloc[i]=currloc;
		tmp=bucket[i];
		tmp*=oar->bucket_sector_count[i];
		currloc+=tmp;
		++i;
	}
	oar->shaloc=currloc;

	if(titles==NULL)
	{
		/* Populate filenames generically */
		i=0;
		while(i<oar->filecount)
		{
			oar->file_headers[i].filename=mallocount(45, NULL, NULL);
			bin2ascii(oar->file_headers[i].sha1, oar->file_headers[i].filename, 20);
			strcat(oar->file_headers[i].filename, oar->file_headers[i].type==FILE_TYPE_BIN?".bin":".iso");
			++i;
		}
	}
	else
	{
		/* Read filenames from title file */
		rewind(titles);
		read_titfile(titles, oar->file_headers, oar->filecount);
	}
}
