#ifndef OAR_HEADERS
#define OAR_HEADERS

#ifdef __cplusplus
extern "C" {
#endif

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cryptowrap.h"

#define FILE_HEADER_LEN 40
struct file_header
{
	/* elements of header */
	uint32_t type;/*  bin/iso */
	uint64_t sector_count;
	uint64_t enc_len;
	uint8_t sha1[20];

	/* variables */
	struct sector *sectors;
	char *filename;
	char *ybe_data;
	uint64_t encloc;
	uint64_t dltloc;
};

struct file_list
{
	char *filename;
	struct file_list *next;
};

#define OAR_HEADER_LEN 40
#define OAR_FOOTER_LEN (32*7)
/* Must be >=2352 */
#define OAR_SCRATCH_LEN (65536)
#define PATH_STRING_LEN (65536)
#define FILENAME_STRING_LEN (1024)
struct oar_header
{
	/* Elements of header */
	uint8_t magic[4];/* "OAR\0" */
	uint32_t version;/* Version of the archive */
	uint64_t oar_filesize;/* Filesize excluding optional sha256 footer */
	uint64_t filecount;/* Number of images contained in archive */
	uint32_t bucket_sector_count[4];/* The number of sectors each bucket contains */

	/* elements of footer */
	uint8_t sha256[OAR_FOOTER_LEN];/* SHA256 hash of every element of header in order (header, dlt, enc, 4xbucket) */

	struct file_header *file_headers;/* Array of structs holding information for an image */

	/* Variables follow */
	uint32_t program_version;/* Version of the program. If less than archive version abort */
	uint32_t total_sectors;/* Total sector count of images, populated when file_header.sector_count is */

	/* Locations of elements in a loaded oar archive */
	uint64_t padloc;
	uint64_t usbloc[4];
	uint64_t shaloc;
};

uint64_t get_enc_size(struct oar_header *oar);
uint64_t get_header_size(struct oar_header *oar);
uint64_t get_calculated_filesize(struct oar_header *oar);

int init_oar_header(struct oar_header *oar);
int free_oar_header(struct oar_header *oar);
/* Print human-readable header to stdout */
void print_header(struct oar_header *oar);


/* Read+hash+populate file_header structs */
void fread_file_headers(struct oar_header *oar, oar_sha256_ctx *sha256, FILE *fin, uint64_t *currloc);

/* Read+hash+populate oar struct */
void fread_header(struct oar_header *oar, oar_sha256_ctx *sha256, FILE *fin, uint64_t *currloc);

int write_oar_header(struct oar_header *oar, FILE *fout, oar_sha256_ctx *sha256);
int write_file_header(struct file_header *fh, FILE *fout, oar_sha256_ctx *sha256);

/* Read/write title file */
int read_titfile(FILE *fin, struct file_header *file_headers, int filecount);
int write_titfile(FILE *fout, struct file_header *file_headers, int filecount);

/* Populate an oar header with info loaded from an archive */
void load_archive(struct oar_header *oar, FILE *archive, FILE *titles, oar_sha256_ctx *sha256);

/* Options a user can set to do things */
struct oar_options
{
	uint8_t constrained_ram;
	uint8_t test;
	uint8_t verbose;

	int override_outfile;
	char outfile[FILENAME_STRING_LEN];

	int *indexes;
	int indexes_count;

	struct file_list *decode_files;
	struct file_list *encode_files;

	uint32_t encode_files_count;
	uint32_t decode_files_count;

	char *oardir;/* Path of directory containing oar executable, used to read user-defined settings like Wii common keys */

	uint8_t wiicommonkey[16];/* User-supplied wii common key, required to handle most Wii images */
	uint8_t wiikoreancommonkey[16];/* User-supplied wii korean common key. Required to handle korean wii images */
	uint8_t ps3key[16];
	uint8_t ps3iv[16];
};

struct sector
{
	uint32_t id;/* position of sector in sorted concatenated file */
	uint8_t hash[12];/* Sha1 hash truncated to 96 bits for struct to be data-aligned to 64 bits */
};

#ifdef __cplusplus
}
#endif

#endif
