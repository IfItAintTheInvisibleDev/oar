#include "nod.h"
#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int nod_read_header(uint8_t *sec)
{
	/* The correct magic word has to be present for a given console. Wii images also have to have encrypted partitions */
	if(sec[0x018+0]==0x5D && sec[0x018+1]==0x1C && sec[0x018+2]==0x9E && sec[0x018+3]==0xA3 && sec[0x061]==0)
		return FILE_TYPE_WII;
	else if(sec[0x01C+0]==0xC2 && sec[0x01C+1]==0x33 && sec[0x01C+2]==0x9F && sec[0x01C+3]==0x3D)
		return FILE_TYPE_NGC;
	else
		return FILE_TYPE_UNDEFINED;
}

/* Functions to generate padding */

void a100026e0(struct nod *nod)
{
	int i = 0;
	while(i!=0x20)
	{
		nod->buffer[i] ^= nod->buffer[i + 0x1e9];
		i++;
	}
	while (i != 0x209)
	{
		nod->buffer[i] ^= nod->buffer[i - 0x20];
		i++;
	}
}

void a10002710(struct nod *nod, uint32_t sample)
{
	uint32_t temp=0;
	int i=0, j=0;
	while (i != 0x11)
	{
		j=0;
		while(j<0x20)
		{
			sample *= 0x5d588b65;
			temp = (temp >> 1) | (sample++ & 0x80000000);
			++j;
		}
		nod->buffer[i] = temp;
		i++;
	}

	nod->buffer[0x10] ^= (nod->buffer[0] >> 0x9) ^ (nod->buffer[0x10] << 0x17);

	i = 1;
	while (i != 0x1f9)
	{
		nod->buffer[i + 0x10] = ((nod->buffer[i - 1] << 0x17) ^ (nod->buffer[i] >> 0x9)) ^ nod->buffer[i + 0xf];
		i++;
	}
	i=0;
	while(i<3)
	{
		a100026e0(nod);
		++i;
	}
}

void get_junk_block(struct nod *nod, uint32_t blockcount)
{
	int i=0, j=0;
	uint32_t sample=0;

	nod->curr_blockcount=blockcount;

	blockcount = blockcount*8*0x1ef29123;
	while (i != 0x40000)
	{
		if ((i & 0x00007fff) == 0)
		{
			sample = (((((uint32_t)nod->id[2] << 0x8) | nod->id[1]) << 0x10) | ((uint32_t)(nod->id[3] + nod->id[2]) << 0x8)) | (uint32_t)(nod->id[0] + nod->id[1]);
			sample = ((sample ^ nod->disc_number) * 0x260bcd5) ^ blockcount;
			a10002710(nod, sample);
			j = 0x208;
			blockcount += 0x1ef29123;
		}
		j++;
		if (j == 0x209)
		{
			a100026e0(nod);
			j = 0;
		}

		nod->garbage_block[i]     = (uint8_t)(nod->buffer[j] >> 0x18);
		nod->garbage_block[i + 1] = (uint8_t)(nod->buffer[j] >> 0x12);
		nod->garbage_block[i + 2] = (uint8_t)(nod->buffer[j] >> 0x8);
		nod->garbage_block[i + 3] = (uint8_t)nod->buffer[j];
		i += 4;
	}
}

