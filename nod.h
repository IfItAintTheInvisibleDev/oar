
#ifndef OAR_NOD
#define OAR_NOD

#ifdef __cplusplus
extern "C" {
#endif

#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#include "filetype.h"

/** Calculate a junk block */
void get_junk_block(struct nod *nod, uint32_t blockcount);

/* Check sector for a Gamecube/Wii header */
/* Returns: 0 for unknown, 1 for Gamecube, 2 for Wii */
int nod_read_header(uint8_t *sec);

#ifdef __cplusplus
}
#endif

#endif
