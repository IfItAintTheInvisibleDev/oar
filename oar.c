#include "common.h"
#include "decode.h"
#include "encode.h"
#include "whereami/whereami.h"
#include "ybe.h"

#include <ctype.h>
#include <dirent.h>
#include <getopt.h>
#include <stdarg.h>
#include <stdio.h>

/**
 * Add filename to a linked list of filenames, a queue of files to be processed
 */
struct file_list* file_list_add(struct file_list *list, char *filename)
{
	struct file_list *newnode;
	/* Create element */
	newnode=mallocount(sizeof(struct file_list), NULL, NULL);
	newnode->filename=mallocount(strlen(filename)+1, NULL, NULL);
	strcpy(newnode->filename, filename);
	/* Add to start */
	newnode->next=list;

	return newnode;
}

/**
 * Print help text
 */
void help()
{
	printf("OAR - Optical ARchiver\n");
	printf("Efficiently archive related iso/bin images using modelling and de-duplication\n");
	printf("\n");
	printf("Usage: oar [ops] <input>\n");
	printf("\n");
	printf("Encode all images in a directory named 'some_dir' into a single archive named 'some_dir.oar':\n");
	printf(" oar some_dir\n");
	printf("Supply the archive path instead of using the directory name:\n");
	printf(" oar some_dir --outfile archive_name.oar\n");
	printf("Decode images in an archive into the current directory:\n");
	printf(" oar archive.oar\n");
	printf("\n");
	printf("Options:\n");
	printf(" --help, -h\n");
	printf(" Print this help text\n");
	printf("\n");
	printf(" --integrity, -i\n");
	printf(" Perform an integrity check on any input archives instead of decoding\n");
	printf("\n");
	printf(" --outfile some_name.oar, -o some_name.oar\n");
	printf(" Supply an archive name to encode to. Overrides use of input directory to name archive\n");
	printf("\n");
	printf(" --subset 0,1,...,n, -s 0,1,...,n\n");
	printf(" Decode a subset of images defined by the supplied zero-indexed CSV\n");
	printf("\n");
	printf(" --test, -t\n");
	printf(" Do a full decode of any input archives but don't write the images to file\n");
	printf("\n");
	printf(" --version, -v\n");
	printf(" Print version\n");
	printf("\n");
}

void version(uint32_t version)
{
	printf("oar v%"PRId32" beta, ISO/BIN/NGC/PS3/WII edition\n", version);
}

int isdir(char *path)
{
	DIR *dp=0;
	dp=opendir(path);
	if(dp==NULL)
		return 0;
	closedir(dp);
	return 1;
}

/**
 * Parse a directory for images to encode
 */
int get_files_from_dir(struct oar_options *ops, char *dirname, char *supportedfiletypes[], int supportedfiletypescount)
{
	DIR *dp=0;
	struct dirent *ep=0;
	char *str_builder;
	int i=0;

	dp=opendir(dirname);
	assert(dp!=NULL);

	str_builder=mallocount(FILENAME_STRING_LEN, NULL, NULL);

	while ( (ep = readdir (dp)) )
	{
		i=0;
		while(i<supportedfiletypescount)
		{
			if( strlen(ep->d_name)>4 && strstr(ep->d_name+strlen(ep->d_name)-4, supportedfiletypes[i])!=NULL )
			{
				if(strlen(dirname)+strlen(ep->d_name)>FILENAME_STRING_LEN-2) return oar_abort("Error: sprintf overflow detected, use a shorter path, abort\n");
				sprintf(str_builder, "%s/%s", dirname, ep->d_name);
				printf("Add '%s' to encode queue\n", ep->d_name);
				ops->encode_files=file_list_add(ops->encode_files, str_builder);
				ops->encode_files_count++;
			}
			++i;
		}

	}
	closedir(dp);
	if(!ops->override_outfile)
	{
		if(strlen(dirname)>FILENAME_STRING_LEN-1) return oar_abort("Error: sprintf overflow detected, use a shorter path, abort\n");
		strcpy(ops->outfile, dirname);
	}
	free(str_builder);
	return 1;
}

int getasciival(uint8_t c)
{
	int val;
	val=tolower(c);
	if(val>=48 && val<=57)
		return val-48;
	else if(val>=97 && val<=102)
		return val-87;
	else return -1;
}

/* Read an ascii key from file and convert it to binary */
int getkeyfromfile(char *path, uint8_t *key)
{
	int len, i=0, val;
	uint8_t ascii[32];
	FILE *fin;
	fin=fopen(path, "rb");
	if(fin==NULL)
	{
		printf("Warning: Could not read '%s', the user needs to supply an encryption key in this file (ASCII hex form) to be able to encode certain image types\n", path);
		return 0;
	}
	fseekass(fin, 0, SEEK_END);
	len=ftellass(fin);
	assert(len>=32);
	fseekass(fin, 0, SEEK_SET);
	freadass(ascii, 1, 32, fin);
	fclose(fin);
	memset(key, 0, 16);
	while(i<32)
	{
		if((val=getasciival(ascii[i]))==-1)
			return oar_abort("Error: Hex key contains non-hex characters\n");
		key[i/2]+= (val<<(i%2==0?4:0));
		++i;
	}

	return 1;
}

/* Read persistent settings stored in the same directory as the oar executable */
int read_settings(struct oar_options *ops)
{
	char *oarpath=0, *tmppath=0;
	int oarpathlen, oardirlen;

	/* Get path to directory containing oar executable, which is where to look for encryption keys not supplied with the program */
	oarpathlen=wai_getExecutablePath(NULL, 0, NULL);
	oarpath=mallocount(oarpathlen+1, NULL, NULL);
	wai_getExecutablePath(oarpath, oarpathlen, &oardirlen);
	ops->oardir=mallocount(oardirlen+1, NULL, NULL);
	memcpy(ops->oardir, oarpath, oardirlen);
	ops->oardir[oardirlen]=0;

	tmppath=mallocount(strlen(ops->oardir)+32, NULL, NULL);

	sprintf(tmppath, "%s/wiicommon.key", ops->oardir);
	getkeyfromfile(tmppath, ops->wiicommonkey);

	sprintf(tmppath, "%s/wiikoreancommon.key", ops->oardir);
	getkeyfromfile(tmppath, ops->wiikoreancommonkey);

	sprintf(tmppath, "%s/ps3.key", ops->oardir);
	getkeyfromfile(tmppath, ops->ps3key);

	sprintf(tmppath, "%s/ps3.iv", ops->oardir);
	getkeyfromfile(tmppath, ops->ps3iv);

	free(oarpath);
	free(tmppath);

	return 1;
}

/** Parse CSV-list arg into int array. If indexes present populate it, otherwise just count number of valid indexes */
int parsesubset(char *arg, int *indexes)
{
	int ind=0, ret=0, val=0;
	char *tok=0, *endptr, *use;
	use=mallocount(strlen(arg)+1, NULL, NULL);
	strcpy(use, arg);
	while( (tok=(ind==0?strtok(use, ","):strtok(NULL, ",")))!=NULL )
	{
		++ind;
		val=strtol(tok, &endptr, 10);
		if(endptr[0]!=0)
			printf("Warning: Failed to properly parse subset index '%s', ignoring\n", tok);
		else
		{
			if(indexes)
				indexes[ret]=val;
			++ret;
		}
	}
	free(use);
	return ret;
}

int main(int argc, char *argv[])
{
	struct file_list *curr=0;
	struct oar_options *ops=0;
	struct oar_header *oar=0;

	char *supportedfiletypes[]={".iso", ".bin"};
	int supportedfiletypescount=2;

	char *str_builder=0;

	/* getopt commandline handling */
	char *short_options="hio:s:tvV";
	int c=0, donesomething=0, i=0, integrityop=0, option_index=0, retval=0;
	static struct option long_options[]=
	{
		{"help", no_argument, 0, 'h'},
		{"integrity", no_argument, 0, 'i'},
		{"outfile", required_argument, 0, 'o'},
		{"subset", required_argument, 0, 's'},
		{"test", no_argument, 0, 't'},
		{"version", no_argument, 0, 'v'},
		{"verbose", no_argument, 0, 'V'},
		{0, 0, 0, 0}
	};

	str_builder=mallocount(FILENAME_STRING_LEN, NULL, NULL);

	ops=mallocount(sizeof(struct oar_options), NULL, NULL);
	sprintf(ops->outfile, "out");

	read_settings(ops);

	oar=mallocount(sizeof(struct oar_header), NULL, NULL);
	if(!init_oar_header(oar)) return 1+oar_abort("Error: init_oar_header() failed, abort\n");

	while(1)
	{
		c=getopt_long(argc, argv, short_options, long_options, &option_index);
		if(c==-1)
			break;

		switch(c)
		{

		case 'h':
			donesomething=1;
			help();
			break;

		case 'i':
			integrityop=1;
			break;

		case 'o':
		 	ops->override_outfile=1;
			if(strlen(optarg)>FILENAME_STRING_LEN-1) return 1+oar_abort("Error: outfile path is too long, abort\n");
			strcpy(ops->outfile, optarg);
			if(memcmp(ops->outfile+strlen(ops->outfile)-4, ".oar", 4)==0)
				ops->outfile[strlen(ops->outfile)-4]=0;
			break;

		case 's':
			ops->indexes_count=parsesubset(optarg, NULL);
			if(ops->indexes_count)
			{
				ops->indexes=mallocount(ops->indexes_count*sizeof(int), NULL, NULL);
				parsesubset(optarg, ops->indexes);
			}
			break;

		case 't':
			ops->test=1;
			break;

		case 'v':
			donesomething=1;
			version(oar->program_version);
			break;

		case 'V':
			ops->verbose=1;
			break;

		}
	}
	/* Parse non-option arguments */
	while (optind < argc)
	{
		if(isdir(argv[optind]))
		{
			if(!get_files_from_dir(ops, argv[optind], supportedfiletypes, supportedfiletypescount)) return 1+oar_abort("Error: get_files_from_dir() failed, abort\n");
		}
		else if(strlen(argv[optind])>4)
		{
			if( memcmp(argv[optind]+strlen(argv[optind])-4, ".oar", 4)==0 )
			{
				ops->decode_files_count++;
				if(strlen(argv[optind])>FILENAME_STRING_LEN-1) return 1+oar_abort("Error: Decode input path is too long, abort\n");
				strcpy(str_builder, argv[optind]);
				str_builder[strlen(str_builder)-4]=0;
				if(!(ops->decode_files=file_list_add(ops->decode_files, str_builder))) return 1+oar_abort("Error: file_list_add() failed, abort\n");
			}
			else
				printf("Unrecognised argument '%s', ignored\n", argv[optind]);
		}
		else
			printf("Unrecognised argument '%s', ignored\n", argv[optind]);

		++optind;
	}
	/* Perform encode */
	if(ops->encode_files_count)
	{
		donesomething=1;
		if(!encode(oar, ops))
			retval=1;
	}
	/* Process decode queue */
	if(ops->decode_files_count)
	{
		donesomething=1;
		curr=ops->decode_files;
		i=0;
		while(i<ops->decode_files_count)
		{
			if(integrityop)
			{
				if(!integrity(oar, ops, curr->filename))
					retval=1;
			}
			else
			{
				if(!decode(oar, ops, curr->filename))
					retval=1;
			}

			curr=curr->next;
			++i;
		}
	}

	if(!donesomething)
		version(oar->program_version);

	free_oar_header(oar);
	free(ops);
	free(str_builder);

	return retval;
}

