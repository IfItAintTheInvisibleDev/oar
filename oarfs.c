#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "filetypeps3.h"
#include "filetypewii.h"
#include "headers.h"
#include "nod.h"
#include "ybe.h"

/* filetype functions */
struct filetype_operations *fops=0;
char *archive_path=0;
/* archive information */
struct oar_header *oar=0;

FILE *logg=0;
/* File pointer to the archive for reading, one per image to allow for concurrency, maybe */
FILE **io=0;
/* dlt and enc data for any opened images */
uint8_t **dlt=0;
uint8_t **enc=0;
/* Padding handler for NGC and WII */
struct nod **nod=0;
/* Encryption handler for PS3 */
struct ps3 **ps3=0;
/* Group handler for WII */
struct wii **wii=0;
/* RAW sector handler for BIN */
struct ybe **ybe=0;

/* FUSE changes dir when -f not used, potentially messing up paths. Clean them */
char * clean_path(const char *path)
{
	return strrchr(path, '/');
}

/* Search for an image in the loaded archive, returning its index if found, -1 otherwise */
static int path_to_index(const char *path)
{
	int i=0;
	while(i<oar->filecount)
	{
		if(strcmp(path+1, oar->file_headers[i].filename)==0)
			break;
		++i;
	}
	return i==oar->filecount?-1:i;
}

/***** STUBS *****/
/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
int oarfs_statfs(const char *path, struct statvfs *stat)
{
	if(logg)
		fprintf(logg, "statfs(): path='%s'\n", path);
	return 0;
}
/***** STUBS *****/

/** Get file attributes.
 *
 * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
 * ignored.	 The 'st_ino' field is ignored except if the 'use_ino'
 * mount option is given.
 */
static int oarfs_getattr( const char *path, struct stat *st )
{
	int i=0;
	char *cleanpath;
	cleanpath=clean_path(path);
	if(logg)
		fprintf(logg, "getattr(): path='%s'\n", path);

	/* Set id to that of current user, and time to current time */
	st->st_uid = getuid();
	st->st_gid = getgid();
	st->st_atime = time(NULL);
	st->st_mtime = time(NULL);

	if ( strcmp( cleanpath, "/" ) == 0 )
	{
		st->st_mode = S_IFDIR | 0755;
		st->st_nlink = 2;
	}
	else
	{
		st->st_mode = S_IFREG | 0644;
		st->st_nlink = 1;
		st->st_size = 0;

		if((i=path_to_index(cleanpath))==-1)
			return -ENOENT;
		st->st_size = oar->file_headers[i].sector_count;
		st->st_size *= oar->file_headers[i].type==FILE_TYPE_BIN?2352:2048;
	}
	return 0;
}

/** Read directory
 *
 * This supersedes the old getdir() interface.  New applications
 * should use this.
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.  This
 * works just like the old getdir() method.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 */
static int oarfs_readdir(const char *path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
	int i=0;
	char *cleanpath;
	cleanpath=clean_path(path);

	if(logg)
		fprintf(logg, "readdir(): path='%s'\n", path);

	filler(buffer, ".", NULL, 0);
	filler(buffer, "..", NULL, 0);

	if(strcmp( cleanpath, "/" )==0)/* root is the only directory */
	{
		while(i<oar->filecount)/* Populate root with all images in archive */
		{
			filler(buffer, oar->file_headers[i].filename, NULL, 0);
			++i;
		}
	}
	return 0;
}

/** File open operation
 *
 * No creation (O_CREAT, O_EXCL) and by default also no
 * truncation (O_TRUNC) flags will be passed to open(). If an
 * application specifies O_TRUNC, fuse first calls truncate()
 * and then open(). Only if 'atomic_o_trunc' has been
 * specified and kernel version is 2.6.24 or later, O_TRUNC is
 * passed on to open.
 *
 * Unless the 'default_permissions' mount option is given,
 * open should check if the operation is permitted for the
 * given flags. Optionally open may also return an arbitrary
 * filehandle in the fuse_file_info structure, which will be
 * passed to all file operations.
 *
 * Changed in version 2.2
 */
static int oarfs_open(const char *path, struct fuse_file_info *fi)
{
	uint8_t *buf=0;
	int i=0;
	char *cleanpath;
	cleanpath=clean_path(path);

	if(logg)
		fprintf(logg, "open(): path='%s'\n", path);

	if((i=path_to_index(cleanpath))==-1)
		return -EBADF;

	if(dlt[i]==NULL)/* DLT should always be present when loaded, so presence can be used to determine if image is loaded */
	{
		io[i]=fopenass(archive_path, "rb");
		load_image(&oar->file_headers[i], &dlt[i], &enc[i], io[i]);
		if(oar->file_headers[i].type==FILE_TYPE_BIN)
		{
			ybe[i]=mallocount(sizeof(struct ybe), NULL, NULL);
			ybe_init(ybe[i]);
		}
		else if(oar->file_headers[i].type==FILE_TYPE_NGC || oar->file_headers[i].type==FILE_TYPE_WII)
		{
			nod[i]=mallocount(sizeof(struct nod), NULL, NULL);
			/* Seed nod struct with data in the first sector */
			buf=mallocount(2048, NULL, NULL);
			dataraw(0, dlt[i], io[i], 2048, oar->usbloc[0], buf);
			memcpy(nod[i]->id, buf, 4);
			nod[i]->disc_number=buf[6];
			get_junk_block(nod[i], 0);

			if(oar->file_headers[i].type==FILE_TYPE_WII)
			{
				wii[i]=mallocount(sizeof(struct wii), NULL, NULL);
				/* Seed wii struct with data in enc */
				populate_partition_table(enc[i], wii[i]);
			}
		}
		else if(oar->file_headers[i].type==FILE_TYPE_PS3)
		{
			ps3[i]=mallocount(sizeof(struct ps3), NULL, NULL);
			/* Seed ps3 struct with data in the first sector */
			buf=mallocount(2048, NULL, NULL);
			dataraw(0, dlt[i], io[i], 2048, oar->usbloc[0], buf);
			populate_encryption_map(buf, ps3[i]);
		}
	}
	if(buf)
		free(buf);
	return 0;
}

/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.	 It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
static int oarfs_release(const char *path, struct fuse_file_info *fi)
{
	int i=0;
	char *cleanpath;
	cleanpath=clean_path(path);

	if(logg)
		fprintf(logg, "release(): path='%s'\n", path);

	if((i=path_to_index(cleanpath))==-1)
		return -EBADF;

	if(io[i])
	{
		fclose(io[i]);
		io[i]=NULL;
	}
	if(dlt[i])
	{
		free(dlt[i]);
		dlt[i]=NULL;
	}
	if(enc[i])
	{
		free(enc[i]);
		enc[i]=NULL;
	}
	if(nod[i])
	{
		free(nod[i]);
		nod[i]=NULL;
	}
	if(ps3[i])
	{
		free(ps3[i]);
		ps3[i]=NULL;
	}
	if(wii[i])
	{
		free(wii[i]);
		wii[i]=NULL;
	}
	if(ybe[i])
	{
		free(ybe[i]);
		ybe[i]=NULL;
	}
	return 0;
}

/** Read data from an open file
 *
 * Read should return exactly the number of bytes requested except
 * on EOF or error, otherwise the rest of the data will be
 * substituted with zeroes.	 An exception to this is when the
 * 'direct_io' mount option is specified, in which case the return
 * value of the read system call will reflect the return value of
 * this operation.
 *
 * Changed in version 2.2
 */
static int oarfs_read(const char *path, char *buffer, size_t size, off_t offset, struct fuse_file_info *fi)
{
	int i=0;
	char *cleanpath;
	cleanpath=clean_path(path);

	if(logg)
		fprintf(logg, "read(): path='%s' size=%"PRIu64" offset=%"PRIu64"\n", path, (uint64_t)size, (uint64_t)offset);

	if((i=path_to_index(cleanpath))==-1)
		return -EBADF;

	return fops[oar->file_headers[i].type].read(&oar->file_headers[i], dlt[i], enc[i], io[i], oar->usbloc, buffer, size, offset, nod[i], ps3[i], wii[i], ybe[i]);
}

struct fuse_operations oarfs_operations =
{
	.getattr = oarfs_getattr,
	.readdir = oarfs_readdir,
	.read = oarfs_read,
	.open = oarfs_open,
	.release = oarfs_release,

	.statfs = oarfs_statfs,
};

int main(int argc, char *argv[])
{
	int fuse_argc=6, i;
	char *fuse_argv[7]={NULL, NULL, "-s", "-f", "-o", "ro", NULL};
	char *str_builder=0;
	FILE *fin=0, *tin=0;

	assert(sizeof(long)==8);

	/*
		To workaround use of -f which doesn't detach from shell, fork
		and work from the child process
	*/
	pid_t pid = fork();
	if (pid == -1)
	{
		printf("fork failed\n");
		exit(EXIT_FAILURE);
	}
	else if (pid !=0)
		exit(0);

	printf("oarfs: FUSE implementation for mounting oar archives\n");
	if(argc<3 || argc>4)
		return oar_abort("Usage: oarfs archive mount_point [log_file]\n");
	fuse_argv[0]=argv[0];
	fuse_argv[1]=argv[2];
	if(argc==4)
		logg=fopenass(argv[3], "wb");

	str_builder=mallocount(PATH_STRING_LEN, NULL, NULL);

	oar=mallocount(sizeof(struct oar_header), NULL, NULL);
	if(!init_oar_header(oar))
		return 1+oar_abort("Error: init_oar_header() failed, abort\n");

	/* Arg validation */
	if(strlen(argv[1])<4 || (strcmp(argv[1]+strlen(argv[1])-4, ".tit")!=0 && strcmp(argv[1]+strlen(argv[1])-4, ".oar")!=0 ) )
		return 1+oar_abort("Error: Argument has an invalid filename for an archive\n");
	assert(1+strlen(argv[1])<PATH_STRING_LEN);

	/* Init function pointers */
	printf("Populate filetype function pointers\n");
	fops=mallocount(FILE_TYPE_OP_ARRAY_SIZE*sizeof(struct filetype_operations), NULL, NULL);
	populate_filetype_ops(fops);

	/* Open archive */
	printf("Load archive\n");
	strcpy(str_builder, argv[1]);
	str_builder[strlen(str_builder)-4]=0;
	strcat(str_builder, ".oar");
	fin=fopenass(str_builder, "rb");
	archive_path=mallocount(strlen(str_builder)+1, NULL, NULL);
	strcpy(archive_path, str_builder);

	str_builder[strlen(str_builder)-4]=0;
	strcat(str_builder, ".tit");
	tin=fopen(str_builder, "rb");

	load_archive(oar, fin, tin, NULL);
	fclose(fin);
	if(tin!=NULL)
		fclose(tin);

	/* Init space for working data for images to be loaded */
	dlt=mallocount(oar->filecount*sizeof(uint8_t *), NULL, NULL);
	enc=mallocount(oar->filecount*sizeof(uint8_t *), NULL, NULL);

	/* Private file pointers to allow for safe concurrency */
	io=mallocount(oar->filecount*sizeof(FILE *), NULL, NULL);

	/* Private instances of any structures needed to decode special image types */
	nod=mallocount(oar->filecount*sizeof(struct nod *), NULL, NULL);
	ps3=mallocount(oar->filecount*sizeof(struct ps3 *), NULL, NULL);
	wii=mallocount(oar->filecount*sizeof(struct wii *), NULL, NULL);
	ybe=mallocount(oar->filecount*sizeof(struct ybe *), NULL, NULL);

	free(str_builder);

	/* Pass control to FUSE */
	printf("Pass control to FUSE: ");
	i=0;
	while(i<fuse_argc)
	{
		printf("%s ", fuse_argv[i]);
		++i;
	}
	printf("\n");

	return fuse_main(fuse_argc, fuse_argv, &oarfs_operations, NULL);
}
