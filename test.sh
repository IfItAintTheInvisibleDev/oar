#!/bin/bash

#Build binaries if they don't exist
if [ ! -f genbin ]
then
	make genbin
fi

rm *.o

if [ ! -f oar ]
then
	make oar
fi

if [ ! -f oar-mbedtls ]
then
	make oar-mbedtls
fi

if [ ! -f oar.exe ]
then
	make oar.exe
fi

if [ ! -f oar-arm ]
then
	make oar-arm
fi

binaries=( "./oar" "./oar-mbedtls" "./oar.exe" "./oar-arm" )

#counters for how many oar tests returned success
respass=0
resfail=0

#Generate testdir if it doesn't exist
if [ ! -d testdir ]
then
	mkdir testdir && cd testdir && ../genbin && cd ../
fi

#Perform tests
for b in "${binaries[@]}"
do
	rescurr=0
	for f in testdir/*
	do
		if [ -d ${f} ]; then
			#Remove existing results of this binary/test combo if it exists
			if [ -d $f/$b ]
			then
				rm -r $f/$b
				rm $f/$b.oar
			fi
			mkdir $f/$b
			#encode
			$b $f -o$f/$b.oar
			if [ $? -eq 0 ]
			then
				((respass++))
			else
				((resfail++))
			fi
			#integrity
			$b -i $f/$b.oar
			if [ $? -eq 0 ]
			then
				((respass++))
			else
				((resfail++))
			fi
			#decode
			cd $f/$b
			../../../$b ../$b.oar
			if [ $? -eq 0 ]
			then
				((respass++))
			else
				((resfail++))
			fi
			cd ../../..
		fi
	done
	echo "$b failed $rescurr tests"
done

echo "$respass tests passed, $resfail tests failed"

sha1sum testdir/*/*.oar

