/*
	Model the 2352 byte yellow book format
	Based on ECM error code modeller by Neill Corlett
*/

#ifndef OAR_YELLOW_BOOK_ENCODING
#define OAR_YELLOW_BOOK_ENCODING

#ifdef __cplusplus
extern "C" {
#endif

#include "common.h"
#include "filetype.h"

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
	Sector types:
		M0: sync address mode=0 data2336
		M1: sync address mode=1 data2048 edc intermediate ecc
		M2: sync address mode=2 data2336
		M2F1: sync address mode=2 subheader data2048 edc ecc
		M2F2: sync address mode=2 subheader data2324 edc
		RAW: data2352

	Sector types bits used:
		M0: 4 for type, 1 for address, 1 for scrambled
		M1: 2 for type, 1 for address, 1 for scrambled, 1 for edc, 1 for intermediate, 2 for ecc
		M2: 4 for type, 1 for address, 1 for scrambled
		M2F1: 2 for type, 1 for address, 1 for scrambled, 1 for subheader, 1 for edc, 2 for ecc
		RAW: 4 for type

		M1 and M2F1 have no spare room for extra flags, the other types have at least
		one bit spare and there is a spare subtype spot for a new sector type
*/

/* flag locations defining the sector type byte */

/* 2 bit type */
#define YBE_FLAG_TYPE_M1 0
#define YBE_FLAG_TYPE_M2F1 1
#define YBE_FLAG_TYPE_M2F2 2
#define YBE_FLAG_TYPE_OTHER 3

/* 2 bit subtype, for rarer modes encapsulated in YBE_FLAG_SECTOR_OTHER */
#define YBE_FLAG_SUBTYPE_M0 0
#define YBE_FLAG_SUBTYPE_RAW 1
#define YBE_FLAG_SUBTYPE_M2 2
#define YBE_FLAG_SUBTYPE_UNASSIGNED 3

/* The following cannot be in locations 2 or 3 as they are fields that subtypes use: DATA, ADDRESS */

/* Set if input is scrambled */
#define YBE_FLAG_SCRAMBLED 7

#define YBE_FLAG_ADDRESS 6

/* Subheader and intermediate are mutually exclusive so can share the same flag */
#define YBE_FLAG_SUBHEADER 5
#define YBE_FLAG_INTERMEDIATE 5

/* Data and EDC are mutually exclusive so can share the same flag */
#define YBE_FLAG_EDC 4

/* None of the subtypes use ECC so the flags can be located where the 2-bit subtype would otherwise be */
#define YBE_FLAG_ECC_Q 3
#define YBE_FLAG_ECC_P 2

/* internal ECM functions */
void eccedc_init(void);
uint32_t edc_compute(uint32_t edc, const uint8_t* src, size_t size);
int8_t ecc_checkpq(const uint8_t* address, const uint8_t* data, size_t major_count, size_t minor_count, size_t major_mult, size_t minor_inc, const uint8_t* ecc);
void ecc_writepq( const uint8_t* address, const uint8_t* data, size_t major_count, size_t minor_count, size_t major_mult, size_t minor_inc, uint8_t* ecc);
int8_t ecc_checksector(const uint8_t *address, const uint8_t *data, const uint8_t *ecc);
void ecc_writesector(const uint8_t *address, const uint8_t *data, uint8_t *ecc);

/* Initialise the global struct containing static data and the predictor */
void ybe_free(struct ybe *g);
void ybe_init(struct ybe *g);

/* Functions to convert a BCD address to/from an integer */
int bcd_to_int(unsigned char *b);
void int_to_bcd(int *i, unsigned char *b);
void add_to_sec(uint32_t *sec, unsigned char *add);
void sec_to_add(uint32_t *sec, unsigned char *add);

/* Read type byte in various ways */
void ybe_type_print(uint8_t type);
int ybe_type_to_data_len(uint8_t type);
int ybe_type_to_enc_len(uint8_t type);
void ybe_type_stat_collector(uint8_t type, uint64_t *stats);

/* Stat collecting function */
int is_eccp_zero(struct ybe *ybe);
int is_eccq_zero(struct ybe *ybe);

/* encode and decode a sector with the input data already read into g as appropriate */
int decode_sector(struct ybe *g);
int encode_sector(struct ybe *g);

#ifdef __cplusplus
}
#endif

#endif
